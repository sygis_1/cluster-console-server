package com.ruoyi.web.controller.system;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/gismap")
public class SyGisMapController extends BaseController {

    @Autowired
    private ISyProjectService projectService;

    @Autowired
    private ISyEngineerService engineerService;

    @Autowired
    private ISySubEngineerService subEngineerService;

    @Autowired
    private ISyStationService stationService;

    @Autowired
    private ISyDatasetInfoService datasetInfoService;

    @Autowired
    private ISyObjectInfoService objectInfoService;

    @Autowired
    private ISyComponentInfoService componentInfoService;

    @GetMapping("/getTreeRootData")
    public AjaxResult getTreeRootData(){
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<SysRole> roles = user.getRoles();
        List<String> roleCollect = roles.stream().map(role -> role.getRoleKey()).collect(Collectors.toList());
        logger.info("-----roles------"+roles);
        logger.info("-----roleCollect------"+roleCollect);
        logger.info("-----user is admin------"+user.isAdmin());
        List<Map<Object,Object>> res = new ArrayList<>();
        if(user.isAdmin()){
            // 项目
            Map<Object,Object> projectMap = new HashMap<>();
            List<SyProject> projects = projectService.selectSyProjectList(new SyProject());
            if(projects.size()>0){
                int projectIndex = 0;
                for(SyProject project:projects){
                    projectIndex = projectIndex+1;
                    List<SyDatasetInfo> datasetInfos = datasetInfoService.selectDatasetByProjectId(project.getUuid());
                    if(datasetInfos.size()>0){
                        int datasetIndex = 0;
                        for(SyDatasetInfo dataset:datasetInfos){
                            datasetIndex = datasetIndex+1;
                            List<SyObjectInfo> objects = objectInfoService.selectSyObjectsByDatasetId(dataset.getUuid());
                            logger.info("");
                            dataset.setName(datasetIndex+"."+dataset.getName()+"("+objects.size()+")");
                            dataset.setLevel(3);
                            if(objects.size()>0){
                                int objectIndex = 0;
                                for (SyObjectInfo object:objects){
                                    objectIndex = objectIndex+1;
                                    object.setName(objectIndex+"."+object.getName());
                                    object.setLevel(4);
                                }
                            }
                            dataset.setChildren(objects);
                        }
                    }
                    project.setName(projectIndex+"."+project.getName()+"("+datasetInfos.size()+")");
                    project.setChildren(datasetInfos);
                    project.setLevel(2);
                }
            }
            projectMap.put("name","1.项目("+projects.size()+")");
            projectMap.put("uuid","1");
            projectMap.put("children",projects);
            projectMap.put("level","1");
            res.add(projectMap);
            // 工程
            Map<Object,Object> engineerMap = new HashMap<>();
            List<SyEngineer> engineers = engineerService.selectSyEngineerList(new SyEngineer());
            if(engineers.size()>0){
                int engineerIndex = 0;
                for(SyEngineer engineer:engineers){
                    engineerIndex = engineerIndex+1;
                    List<SyDatasetInfo> datasetInfos = datasetInfoService.selectDatasetByEngineerId(engineer.getUuid());
                    if(datasetInfos.size()>0){
                        int datasetIndex = 0;
                        for(SyDatasetInfo dataset:datasetInfos){
                            datasetIndex = datasetIndex+1;
                            List<SyObjectInfo> objects = objectInfoService.selectSyObjectsByDatasetId(dataset.getUuid());
                            logger.info("");
                            dataset.setName(datasetIndex+"."+dataset.getName()+"("+objects.size()+")");
                            dataset.setLevel(3);
                            if(objects.size()>0){
                                int objectIndex = 0;
                                for (SyObjectInfo object:objects){
                                    objectIndex = objectIndex+1;
                                    object.setName(objectIndex+"."+object.getName());
                                    object.setLevel(4);
                                }
                            }
                            dataset.setChildren(objects);
                        }
                    }
                    engineer.setName(engineerIndex+"."+engineer.getName()+"("+datasetInfos.size()+")");
                    engineer.setChildren(datasetInfos);
                    engineer.setLevel(2);
                }
            }
            engineerMap.put("name","2.工程("+engineers.size()+")");
            engineerMap.put("uuid","2");
            engineerMap.put("children",engineers);
            engineerMap.put("level","1");
            res.add(engineerMap);
            // 子工程
            Map<Object,Object> subEngineerMap = new HashMap<>();
            List<SySubEngineer> subEngineers = subEngineerService.selectSySubEngineerList(new SySubEngineer());
            if(subEngineers.size()>0){
                int subEngineerIndex = 0;
                for(SySubEngineer subEngineer:subEngineers){
                    subEngineerIndex = subEngineerIndex+1;
                    List<SyDatasetInfo> datasetInfos = datasetInfoService.selectDatasetBySubEngineerId(subEngineer.getUuid());
                    if(datasetInfos.size()>0){
                        int datasetIndex = 0;
                        for(SyDatasetInfo dataset:datasetInfos){
                            datasetIndex = datasetIndex+1;
                            List<SyObjectInfo> objects = objectInfoService.selectSyObjectsByDatasetId(dataset.getUuid());
                            logger.info("");
                            dataset.setName(datasetIndex+"."+dataset.getName()+"("+objects.size()+")");
                            dataset.setLevel(3);
                            if(objects.size()>0){
                                int objectIndex = 0;
                                for (SyObjectInfo object:objects){
                                    objectIndex = objectIndex+1;
                                    object.setName(objectIndex+"."+object.getName());
                                    object.setLevel(4);
                                }
                            }
                            dataset.setChildren(objects);
                        }
                    }
                    subEngineer.setName(subEngineerIndex+"."+subEngineer.getName()+"("+datasetInfos.size()+")");
                    subEngineer.setChildren(datasetInfos);
                    subEngineer.setLevel(2);
                }
            }
            subEngineerMap.put("name","3.子工程("+subEngineers.size()+")");
            subEngineerMap.put("uuid","3");
            subEngineerMap.put("children",subEngineers);
            subEngineerMap.put("level","1");
            res.add(subEngineerMap);
            // 站点
            Map<Object,Object> stationMap = new HashMap<>();
            List<SyStation> stations = stationService.selectSyStationList(new SyStation());
            if(stations.size()>0){
                int stationIndex = 0;
                for(SyStation station:stations){
                    stationIndex = stationIndex+1;
                    List<SyDatasetInfo> datasetInfos = datasetInfoService.selectDatasetByStationId(station.getUuid());
                    if(datasetInfos.size()>0){
                        int datasetIndex = 0;
                        for(SyDatasetInfo dataset:datasetInfos){
                            datasetIndex = datasetIndex+1;
                            List<SyObjectInfo> objects = objectInfoService.selectSyObjectsByDatasetId(dataset.getUuid());
                            logger.info("");
                            dataset.setName(datasetIndex+"."+dataset.getName()+"("+objects.size()+")");
                            dataset.setLevel(3);
                            if(objects.size()>0){
                                int objectIndex = 0;
                                for (SyObjectInfo object:objects){
                                    objectIndex = objectIndex+1;
                                    object.setName(objectIndex+"."+object.getName());
                                    object.setLevel(4);
                                }
                            }
                            dataset.setChildren(objects);
                        }
                    }
                    station.setName(stationIndex+"."+station.getName()+"("+datasetInfos.size()+")");
                    station.setChildren(datasetInfos);
                    station.setLevel(2);
                }
            }
            stationMap.put("name","4.站点("+stations.size()+")");
            stationMap.put("uuid","4");
            stationMap.put("children",stations);
            stationMap.put("level","1");
            res.add(stationMap);

        }else{

        }
        return AjaxResult.success(res);
    }
}
