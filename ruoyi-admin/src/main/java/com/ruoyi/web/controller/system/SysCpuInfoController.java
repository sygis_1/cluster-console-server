package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysCpuInfo;
import com.ruoyi.system.service.ISysCpuInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * cpu信息Controller
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/system/cpu")
public class SysCpuInfoController extends BaseController {

    @Autowired
    private ISysCpuInfoService sysCpuInfoService;

    /**
     * 查询cpu信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysCpuInfo sysCpuInfo)
    {
        startPage();
        List<SysCpuInfo> list = sysCpuInfoService.selectSysCpuInfoList(sysCpuInfo);
        return getDataTable(list);
    }

    /**
     * 导出cpu信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:export')")
    @Log(title = "cpu信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysCpuInfo sysCpuInfo)
    {
        List<SysCpuInfo> list = sysCpuInfoService.selectSysCpuInfoList(sysCpuInfo);
        ExcelUtil<SysCpuInfo> util = new ExcelUtil<SysCpuInfo>(SysCpuInfo.class);
        util.exportExcel(response, list, "cpu信息数据");
    }

    /**
     * 获取cpu信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:query')")
    @GetMapping(value = "/{serverIp}")
    public AjaxResult getInfo(@PathVariable("serverIp") String serverIp)
    {
        return success(sysCpuInfoService.selectSysCpuInfoByServerIp(serverIp));
    }

    /**
     * 新增cpu信息
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:add')")
    @Log(title = "cpu信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCpuInfo sysCpuInfo)
    {
        return toAjax(sysCpuInfoService.insertSysCpuInfo(sysCpuInfo));
    }

    /**
     * 修改cpu信息
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:edit')")
    @Log(title = "cpu信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCpuInfo sysCpuInfo)
    {
        return toAjax(sysCpuInfoService.updateSysCpuInfo(sysCpuInfo));
    }

    /**
     * 删除cpu信息
     */
    @PreAuthorize("@ss.hasPermi('system:cpu:remove')")
    @Log(title = "cpu信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{serverIps}")
    public AjaxResult remove(@PathVariable String[] serverIps)
    {
        return toAjax(sysCpuInfoService.deleteSysCpuInfoByServerIps(serverIps));
    }
}
