package com.ruoyi.web.controller.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "minio.config")
public class MinioProperties {
    private String ip;

    /**
     * API调用地址端口
     */
    private Integer port;

    /**
     * 连接账号
     */
    private String accessKey;

    /**
     * 连接秘钥
     */
    private String secretKey;

    /**
     * #如果是true，则用的是https而不是http,默认值是true
     */
    private Boolean secure;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Boolean getSecure() {
        return secure;
    }

    public void setSecure(Boolean secure) {
        this.secure = secure;
    }
}
