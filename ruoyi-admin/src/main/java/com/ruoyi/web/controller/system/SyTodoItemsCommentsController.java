package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyTodoItemsComments;
import com.ruoyi.system.service.ISyTodoItemsCommentsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 待办事项追述Controller
 * 
 * @author Sy
 * @date 2023-10-08
 */
@RestController
@RequestMapping("/system/comments")
public class SyTodoItemsCommentsController extends BaseController {

    @Autowired
    private ISyTodoItemsCommentsService syTodoItemsCommentsService;

    /**
     * 查询待办事项追述列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyTodoItemsComments syTodoItemsComments) {
        startPage();
        List<SyTodoItemsComments> list = syTodoItemsCommentsService.selectSyTodoItemsCommentsList(syTodoItemsComments);
        return getDataTable(list);
    }

    /**
     * 导出待办事项追述列表
     */
    @Log(title = "待办事项追述", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyTodoItemsComments syTodoItemsComments) {
        List<SyTodoItemsComments> list = syTodoItemsCommentsService.selectSyTodoItemsCommentsList(syTodoItemsComments);
        ExcelUtil<SyTodoItemsComments> util = new ExcelUtil<SyTodoItemsComments>(SyTodoItemsComments.class);
        util.exportExcel(response, list, "待办事项追述数据");
    }

    /**
     * 获取待办事项追述详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(syTodoItemsCommentsService.selectSyTodoItemsCommentsByUuid(uuid));
    }

    /**
     * 新增待办事项追述
     */
    @Log(title = "待办事项追述", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyTodoItemsComments syTodoItemsComments) {
        return toAjax(syTodoItemsCommentsService.insertSyTodoItemsComments(syTodoItemsComments));
    }

    /**
     * 修改待办事项追述
     */
    @Log(title = "待办事项追述", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyTodoItemsComments syTodoItemsComments) {
        return toAjax(syTodoItemsCommentsService.updateSyTodoItemsComments(syTodoItemsComments));
    }

    /**
     * 删除待办事项追述
     */
    @Log(title = "待办事项追述", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syTodoItemsCommentsService.deleteSyTodoItemsCommentsByUuids(uuids));
    }
}
