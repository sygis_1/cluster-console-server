package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysGpuInfo;
import com.ruoyi.system.service.ISysGpuInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * GPU信息表Controller
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/system/gpu")
public class SysGpuInfoController extends BaseController {

    @Autowired
    private ISysGpuInfoService sysGpuInfoService;

    /**
     * 查询GPU信息表列表
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysGpuInfo sysGpuInfo)
    {
        startPage();
        List<SysGpuInfo> list = sysGpuInfoService.selectSysGpuInfoList(sysGpuInfo);
        return getDataTable(list);
    }

    /**
     * 导出GPU信息表列表
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:export')")
    @Log(title = "GPU信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysGpuInfo sysGpuInfo)
    {
        List<SysGpuInfo> list = sysGpuInfoService.selectSysGpuInfoList(sysGpuInfo);
        ExcelUtil<SysGpuInfo> util = new ExcelUtil<SysGpuInfo>(SysGpuInfo.class);
        util.exportExcel(response, list, "GPU信息表数据");
    }

    /**
     * 获取GPU信息表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:query')")
    @GetMapping(value = "/{serverIp}")
    public AjaxResult getInfo(@PathVariable("serverIp") String serverIp)
    {
        return success(sysGpuInfoService.selectSysGpuInfoByServerIp(serverIp));
    }

    /**
     * 新增GPU信息表
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:add')")
    @Log(title = "GPU信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysGpuInfo sysGpuInfo)
    {
        return toAjax(sysGpuInfoService.insertSysGpuInfo(sysGpuInfo));
    }

    /**
     * 修改GPU信息表
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:edit')")
    @Log(title = "GPU信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysGpuInfo sysGpuInfo)
    {
        return toAjax(sysGpuInfoService.updateSysGpuInfo(sysGpuInfo));
    }

    /**
     * 删除GPU信息表
     */
    @PreAuthorize("@ss.hasPermi('system:gpu:remove')")
    @Log(title = "GPU信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{serverIps}")
    public AjaxResult remove(@PathVariable String[] serverIps)
    {
        return toAjax(sysGpuInfoService.deleteSysGpuInfoByServerIps(serverIps));
    }
}
