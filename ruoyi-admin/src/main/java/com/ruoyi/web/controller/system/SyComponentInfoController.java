package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SyDatasetInfo;
import com.ruoyi.system.domain.SyObjectInfo;
import com.ruoyi.system.service.ISyDatasetInfoService;
import com.ruoyi.system.service.ISyObjectInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyComponentInfo;
import com.ruoyi.system.service.ISyComponentInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 部件表Controller
 * 
 * @author Sy
 * @date 2023-09-28
 */
@RestController
@RequestMapping("/system/component")
public class SyComponentInfoController extends BaseController {

    @Autowired
    private ISyComponentInfoService syComponentInfoService;

    @Autowired
    private ISyObjectInfoService objectInfoService;

    /**
     * 查询部件表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyComponentInfo syComponentInfo) {
        startPage();
        List<SyComponentInfo> list = syComponentInfoService.selectSyComponentInfoList(syComponentInfo);
        return getDataTable(list);
    }

    @GetMapping("/listComponents")
    public AjaxResult listComponents(SyComponentInfo syComponentInfo) {
        List<SyComponentInfo> list = syComponentInfoService.selectSyComponentInfoList(syComponentInfo);
        return AjaxResult.success(list);
    }

    /**
     * 导出部件表列表
     */
    @Log(title = "部件表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyComponentInfo syComponentInfo) {
        List<SyComponentInfo> list = syComponentInfoService.selectSyComponentInfoList(syComponentInfo);
        ExcelUtil<SyComponentInfo> util = new ExcelUtil<SyComponentInfo>(SyComponentInfo.class);
        util.exportExcel(response, list, "部件表数据");
    }

    /**
     * 获取部件表详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(syComponentInfoService.selectSyComponentInfoByUuid(uuid));
    }

    /**
     * 新增部件表
     */
    @Log(title = "部件表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyComponentInfo syComponentInfo) {
        return toAjax(syComponentInfoService.insertSyComponentInfo(syComponentInfo));
    }

    @PostMapping("/importComponent")
    public AjaxResult importComponent(@RequestBody SyComponentInfo[] components) {
        if(components.length>0){
            SyObjectInfo objectInfo = objectInfoService.selectSyObjectInfoByUuid(components[0].getObjectId());
            for(SyComponentInfo component:components){
                SyComponentInfo param = new SyComponentInfo();
                param.setDatasetId(component.getDatasetId());
                param.setObjectId(component.getObjectId());
                param.setOrginName(component.getName());
                List<SyComponentInfo> res = syComponentInfoService.selectSyComponentInfoList(param);
                if(res.size()==0){
                    component.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
                    component.setCreateBy(SecurityUtils.getLoginUser().getUserId().toString());
                    component.setCreateTime(new Date());
                    component.setPath(objectInfo.getPath());
                    component.setOrginName(component.getName());
                    syComponentInfoService.insertSyComponentInfo(component);
                }
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/updateComponentInfo")
    public AjaxResult updateComponentInfo(@RequestBody SyComponentInfo components) {
        components.setUpdateBy(SecurityUtils.getLoginUser().getUserId().toString());
        components.setUpdateTime(new Date());
        syComponentInfoService.updateSyComponentInfo(components);
        return AjaxResult.success();
    }

    @GetMapping("/getComponentInfo")
    public AjaxResult getComponentInfo(SyComponentInfo syComponentInfo){
        List<SyComponentInfo> list = syComponentInfoService.selectSyComponentInfoList(syComponentInfo);
        SyComponentInfo res = new SyComponentInfo();
        if(list.size()>0){
            res = list.get(0);
        }
        return AjaxResult.success(res);
    }

    /**
     * 修改部件表
     */
    @Log(title = "部件表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyComponentInfo syComponentInfo) {
        return toAjax(syComponentInfoService.updateSyComponentInfo(syComponentInfo));
    }

    /**
     * 删除部件表
     */
    @Log(title = "部件表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syComponentInfoService.deleteSyComponentInfoByUuids(uuids));
    }
}
