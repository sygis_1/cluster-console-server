package com.ruoyi.web.controller.system;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.SysCpuInfo;
import com.ruoyi.system.domain.SysGpuInfo;
import com.ruoyi.system.domain.SysMemInfo;
import com.ruoyi.system.service.ISysCpuInfoService;
import com.ruoyi.system.service.ISysGpuInfoService;
import com.ruoyi.system.service.ISysMemInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysEngineInfo;
import com.ruoyi.system.service.ISysEngineInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 引擎信息表Controller
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/system/engine")
public class SysEngineInfoController extends BaseController {

    @Autowired
    private ISysEngineInfoService sysEngineInfoService;

    @Autowired
    private ISysCpuInfoService cpuInfoService;

    @Autowired
    private ISysGpuInfoService gpuInfoService;

    @Autowired
    private ISysMemInfoService memInfoService;

    @GetMapping("/getClientInfo")
    public AjaxResult getClientInfo(SysEngineInfo sysEngineInfo) {

        List<SysEngineInfo> engineInfos = sysEngineInfoService.selectSysEngineInfoList(sysEngineInfo);
        Map<Object,Object> res = new HashMap<>();
        Map<Object,Object> engineInfo = new HashMap<>();
        if(engineInfos.size()>0){
            engineInfo.put("cluster_ip",engineInfos.get(0).getClusterIp());
            engineInfo.put("engine_type",engineInfos.get(0).getEngineType());
            engineInfo.put("type_name",engineInfos.get(0).getTypeName());
            engineInfo.put("job_path",engineInfos.get(0).getJobPath());
            engineInfo.put("engine_status",engineInfos.get(0).getEngineStatus());
        }else{
            engineInfo.put("cluster_ip",null);
            engineInfo.put("engine_type",null);
            engineInfo.put("engine_type",null);
            engineInfo.put("job_path",null);
            engineInfo.put("engine_status",null);
        }
        res.put("engine",engineInfo);

        Map<Object,Object> cpuInfo = new HashMap<>();
        SysCpuInfo cpu = new SysCpuInfo();
        cpu.setServerIp(sysEngineInfo.getServerIp());
        List<SysCpuInfo> cpuInfos = cpuInfoService.selectSysCpuInfoList(cpu);
        if(cpuInfos.size()>0){
            cpuInfo.put("cpu_cores",cpuInfos.get(0).getCpuCores());
            cpuInfo.put("cpu_sys_used_rate",cpuInfos.get(0).getCpuSysUsedRate());
            cpuInfo.put("cpu_user_used_rate",cpuInfos.get(0).getCpuUserUsedRate());
            cpuInfo.put("cpu_wait_rate",cpuInfos.get(0).getCpuWaitRate());
            cpuInfo.put("cpu_sum_used_rate",cpuInfos.get(0).getCpuSumUsedRate());
            cpuInfo.put("cpu_degree",0);
        }else{
            cpuInfo.put("cpu_cores",null);
            cpuInfo.put("cpu_sys_used_rate",null);
            cpuInfo.put("cpu_user_used_rate",null);
            cpuInfo.put("cpu_wait_rate",null);
            cpuInfo.put("cpu_sum_used_rate",null);
            cpuInfo.put("cpu_degree",null);
        }
        res.put("cpu",cpuInfo);
        Map<Object,Object> gpuInfo = new HashMap<>();
        SysGpuInfo gpu = new SysGpuInfo();
        gpu.setServerIp(sysEngineInfo.getServerIp());
        List<SysGpuInfo> gpuInfos = gpuInfoService.selectSysGpuInfoList(gpu);
        if(gpuInfos.size()>0){
            gpuInfo.put("name",gpuInfos.get(0).getGpuName());
            gpuInfo.put("number",gpuInfos.get(0).getGpuNo());
            gpuInfo.put("temperature",gpuInfos.get(0).getGpuDegree());
            gpuInfo.put("totalMemory",gpuInfos.get(0).getGpuSumMem());
            gpuInfo.put("usageRate",gpuInfos.get(0).getGpuUsedRate());
            gpuInfo.put("useableMemory",gpuInfos.get(0).getGpuFreeMem());
            gpuInfo.put("usedMemory",gpuInfos.get(0).getGpuUsedMem());
        }else{
            gpuInfo.put("name",null);
            gpuInfo.put("number",null);
            gpuInfo.put("temperature",null);
            gpuInfo.put("totalMemory",null);
            gpuInfo.put("usageRate",null);
            gpuInfo.put("useableMemory",null);
            gpuInfo.put("usedMemory",null);
        }
        res.put("gpu",gpuInfo);
        Map<Object,Object> memInfo = new HashMap<>();
        SysMemInfo mem = new SysMemInfo();
        mem.setServerIp(sysEngineInfo.getServerIp());
        mem.setHostName(sysEngineInfo.getHostName());
        List<SysMemInfo> memInfos = memInfoService.selectSysMemInfoList(mem);
        if(memInfos.size()>0){
            memInfo.put("hostname",memInfos.get(0).getHostName());
            memInfo.put("ip",memInfos.get(0).getServerIp());
            memInfo.put("mem_left",memInfos.get(0).getMemLeft());
            memInfo.put("mem_total",memInfos.get(0).getMemTotal());
            memInfo.put("mem_use_rate",memInfos.get(0).getMemUseRate());
            memInfo.put("mem_used",memInfos.get(0).getMemUsed());
            memInfo.put("sys_arch",memInfos.get(0).getSysArch());
            memInfo.put("sys_name",memInfos.get(0).getSysName());
        }else{
            memInfo.put("hostname",null);
            memInfo.put("ip",null);
            memInfo.put("mem_left",null);
            memInfo.put("mem_total",null);
            memInfo.put("mem_use_rate",null);
            memInfo.put("mem_used",null);
            memInfo.put("sys_arch",null);
            memInfo.put("sys_name",null);
        }
        res.put("mem",memInfo);
        return AjaxResult.success(res);
    }

    /**
     * 查询引擎信息表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysEngineInfo sysEngineInfo) {
        startPage();
        List<SysEngineInfo> list = sysEngineInfoService.selectSysEngineInfoList(sysEngineInfo);
        return getDataTable(list);
    }

    /**
     * 导出引擎信息表列表
     */
    @Log(title = "引擎信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysEngineInfo sysEngineInfo) {
        List<SysEngineInfo> list = sysEngineInfoService.selectSysEngineInfoList(sysEngineInfo);
        ExcelUtil<SysEngineInfo> util = new ExcelUtil<SysEngineInfo>(SysEngineInfo.class);
        util.exportExcel(response, list, "引擎信息表数据");
    }

    /**
     * 获取引擎信息表详细信息
     */
    @GetMapping(value = "/{serverIp}")
    public AjaxResult getInfo(@PathVariable("serverIp") String serverIp) {
        return success(sysEngineInfoService.selectSysEngineInfoByServerIp(serverIp));
    }

    @PostMapping("/addNewEngine")
    public AjaxResult addNewEngine(@RequestBody SysEngineInfo sysEngineInfo) {
        SysEngineInfo engine = new SysEngineInfo();
        engine.setServerIp(sysEngineInfo.getServerIp());
        engine.setHostName(sysEngineInfo.getHostName());
        List<SysEngineInfo> list = sysEngineInfoService.selectSysEngineInfoList(engine);
        sysEngineInfo.setClusterIn(1);
        sysEngineInfo.setInTime(new Date());
        sysEngineInfo.setQuitTime(null);
        if(list.size()>0){
            sysEngineInfoService.updateSysEngineInfo(sysEngineInfo);
        }else{
            sysEngineInfo.setEngineStatus("异常");
            sysEngineInfo.setServerType(2);
            sysEngineInfoService.insertSysEngineInfo(sysEngineInfo);
        }
        return AjaxResult.success();
    }

    /**
     * 新增引擎信息表
     */
    @Log(title = "引擎信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysEngineInfo sysEngineInfo) {
        return toAjax(sysEngineInfoService.insertSysEngineInfo(sysEngineInfo));
    }

    /**
     * 修改引擎信息表
     */
    @Log(title = "引擎信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysEngineInfo sysEngineInfo) {
        return toAjax(sysEngineInfoService.updateSysEngineInfo(sysEngineInfo));
    }

    /**
     * 删除引擎信息表
     */
    @Log(title = "引擎信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{serverIps}")
    public AjaxResult remove(@PathVariable String[] serverIps) {
        return toAjax(sysEngineInfoService.deleteSysEngineInfoByServerIps(serverIps));
    }
}
