package com.ruoyi.web.controller.system;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.ItemParentInfoVo;
import com.ruoyi.system.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 待办事项Controller
 * 
 * @author Sy
 * @date 2023-10-08
 */
@RestController
@RequestMapping("/system/items")
public class SyTodoItemsController extends BaseController {

    @Autowired
    private ISyTodoItemsService syTodoItemsService;

    @Autowired
    private ISyProjectService projectService;

    @Autowired
    private ISyEngineerService engineerService;

    @Autowired
    private ISySubEngineerService subEngineerService;

    @Autowired
    private ISyStationService stationService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISyTodoItemsFollowupService followupService;

    @Autowired
    private ISyTodoItemsCommentsService commentsService;

    @GetMapping("/getitemParentInfoList")
    public AjaxResult getitemParentInfoList(SyTodoItems item){
        List<ItemParentInfoVo> list = new ArrayList<>();
        if(item.getItemType()==1){
            List<SyProject> projects = projectService.selectProjectList4Select();
            if(projects.size()>0){
                for (SyProject project:projects){
                    ItemParentInfoVo vo = new ItemParentInfoVo();
                    vo.setUuid(project.getUuid());
                    vo.setName(project.getName());
                    list.add(vo);
                }
            }
        }else if(item.getItemType()==2){
            List<SyEngineer> engineers = engineerService.selectSyEngineerList4Select();
            if(engineers.size()>0){
                for (SyEngineer engineer:engineers){
                    ItemParentInfoVo vo = new ItemParentInfoVo();
                    vo.setUuid(engineer.getUuid());
                    vo.setName(engineer.getName());
                    list.add(vo);
                }
            }
        }else if(item.getItemType()==3){
            List<SySubEngineer> subEngineers = subEngineerService.selectSubEngineerList4Select();
            if(subEngineers.size()>0){
                for (SySubEngineer subEngineer:subEngineers){
                    ItemParentInfoVo vo = new ItemParentInfoVo();
                    vo.setUuid(subEngineer.getUuid());
                    vo.setName(subEngineer.getName());
                    list.add(vo);
                }
            }
        }else if(item.getItemType()==4){
            List<SyStation> stations = stationService.selectDatasetStationList();
            if(stations.size()>0){
                for (SyStation station:stations){
                    ItemParentInfoVo vo = new ItemParentInfoVo();
                    vo.setUuid(station.getUuid());
                    vo.setName(station.getName());
                    list.add(vo);
                }
            }
        }
        return AjaxResult.success(list);
    }

    @PostMapping("/editItem")
    public AjaxResult editItem(@RequestBody SyTodoItems item) {
        SysUser followUp = userService.selectUserById(item.getFollowupId());
        if(followUp!=null){
            item.setFollowupName(followUp.getNickName());
        }
        if(item.getUuid()!=null){ //修改
            syTodoItemsService.updateSyTodoItems(item);
        }else{ //新增
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            item.setUuid(uuid);
            item.setCreateBy(SecurityUtils.getLoginUser().getUserId().toString());
            item.setCreateName(SecurityUtils.getLoginUser().getUser().getNickName());
            item.setCreateTime(new Date());
            item.setStatus(0);
            syTodoItemsService.insertSyTodoItems(item);
        }
        return AjaxResult.success();
    }

    /**
     * 查询待办事项列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyTodoItems syTodoItems) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<SyTodoItems> list = null;
        if(SecurityUtils.getLoginUser().getUser().isAdmin()){
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList(syTodoItems);
        } else {
            syTodoItems.setCreateBy(user.getUserId().toString());
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList(syTodoItems);
        }
        if(list.size()>0){
            for(SyTodoItems res:list){
                if(user.getUserId()==1){
                    res.setEditFlag(1);//给操作
                }else{
                    if(Long.parseLong(res.getCreateBy())==user.getUserId()){
                        res.setEditFlag(1);//给操作
                    }else{
                        res.setEditFlag(0);//给操作
                    }
                }
                if(res.getItemType()==1){
                    SyProject project = projectService.selectSyProjectByUuid(res.getItemParentId());
                    if(project!=null){
                        res.setItemParentName(project.getName());
                    }
                }else if(res.getItemType()==2){
                    SyEngineer engineer = engineerService.selectSyEngineerByUuid(res.getItemParentId());
                    if(engineer!=null){
                        res.setItemParentName(engineer.getName());
                    }
                }else if(res.getItemType()==3){
                    SySubEngineer subEngineer = subEngineerService.selectSySubEngineerByUuid(res.getItemParentId());
                    if(subEngineer!=null){
                        res.setItemParentName(subEngineer.getName());
                    }
                }else if(res.getItemType()==4){
                    SyStation station = stationService.selectSyStationByUuid(res.getItemParentId());
                    if(station!=null){
                        res.setItemParentName(station.getName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出待办事项列表
     */
    @Log(title = "待办事项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyTodoItems syTodoItems) {
        List<SyTodoItems> list = syTodoItemsService.selectSyTodoItemsList(syTodoItems);
        ExcelUtil<SyTodoItems> util = new ExcelUtil<SyTodoItems>(SyTodoItems.class);
        util.exportExcel(response, list, "待办事项数据");
    }

    /**
     * 获取待办事项详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        SyTodoItems res = syTodoItemsService.selectSyTodoItemsByUuid(uuid);
        if(res!=null){
            if(res.getItemType()==1){
                SyProject project = projectService.selectSyProjectByUuid(res.getItemParentId());
                if(project!=null){
                    res.setItemParentName(project.getName());
                }
            }else if(res.getItemType()==2){
                SyEngineer engineer = engineerService.selectSyEngineerByUuid(res.getItemParentId());
                if(engineer!=null){
                    res.setItemParentName(engineer.getName());
                }
            }else if(res.getItemType()==3){
                SySubEngineer subEngineer = subEngineerService.selectSySubEngineerByUuid(res.getItemParentId());
                if(subEngineer!=null){
                    res.setItemParentName(subEngineer.getName());
                }
            }else if(res.getItemType()==4){
                SyStation station = stationService.selectSyStationByUuid(res.getItemParentId());
                if(station!=null){
                    res.setItemParentName(station.getName());
                }
            }
        }
        return success(res);
    }

    /**
     * 新增待办事项
     */
    @Log(title = "待办事项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyTodoItems syTodoItems) {
        return toAjax(syTodoItemsService.insertSyTodoItems(syTodoItems));
    }

    /**
     * 修改待办事项
     */
    @Log(title = "待办事项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyTodoItems syTodoItems) {
        return toAjax(syTodoItemsService.updateSyTodoItems(syTodoItems));
    }

    /**
     * 删除待办事项
     */
    @Log(title = "待办事项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syTodoItemsService.deleteSyTodoItemsByUuids(uuids));
    }

    @GetMapping("/listItemsForms")
    public AjaxResult listItemsForms(SyTodoItemsFollowup form){
        List<SyTodoItemsFollowup> list = followupService.selectSyTodoItemsFollowupList(form);
        return AjaxResult.success(list);
    }

    @GetMapping("/listItemComments")
    public AjaxResult listItemComments(SyTodoItemsComments param){
        List<SyTodoItemsComments> list = commentsService.selectSyTodoItemsCommentsList(param);
        return AjaxResult.success(list);
    }

    @PostMapping("/addRecout")
    public AjaxResult addRecout(@RequestBody SyTodoItems items){
        SyTodoItemsComments comments = new SyTodoItemsComments();
        comments.setUuid(UUID.randomUUID().toString());
        comments.setItemId(items.getUuid());
        comments.setComment(items.getRemark());
        comments.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
        comments.setCreateName(SecurityUtils.getLoginUser().getUser().getNickName());
        comments.setCreateTime(new Date());
        return toAjax(commentsService.insertSyTodoItemsComments(comments));
    }

    @PostMapping("/addAnnotion")
    public AjaxResult addAnnotion(@RequestBody SyTodoItems param){
        SyTodoItems items = syTodoItemsService.selectSyTodoItemsByUuid(param.getUuid());
        if(items!=null){
            items.setAnnotation(param.getAnnotation());
            items.setStatus(2);
            return toAjax(syTodoItemsService.updateSyTodoItems(items));
        }else{
            return AjaxResult.error("没找到对应的待办事项");
        }
    }

    @GetMapping("/listDoneItems")
    public TableDataInfo listDoneItems(SyTodoItems item){
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<String> roleCollect = user.getRoles().stream().map(role -> role.getRoleKey()).collect(Collectors.toList());
        List<SyTodoItems> list = null;
        item.setStatus(2);
        if(SecurityUtils.getLoginUser().getUser().isAdmin()){
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList(item);
        } else {
            item.setFollowupId(user.getUserId());
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList(item);
        }
        if(list.size()>0){
            for(SyTodoItems res:list){
                if(res.getItemType()==1){
                    SyProject project = projectService.selectSyProjectByUuid(res.getItemParentId());
                    if(project!=null){
                        res.setItemParentName(project.getName());
                    }
                }else if(res.getItemType()==2){
                    SyEngineer engineer = engineerService.selectSyEngineerByUuid(res.getItemParentId());
                    if(engineer!=null){
                        res.setItemParentName(engineer.getName());
                    }
                }else if(res.getItemType()==3){
                    SySubEngineer subEngineer = subEngineerService.selectSySubEngineerByUuid(res.getItemParentId());
                    if(subEngineer!=null){
                        res.setItemParentName(subEngineer.getName());
                    }
                }else if(res.getItemType()==4){
                    SyStation station = stationService.selectSyStationByUuid(res.getItemParentId());
                    if(station!=null){
                        res.setItemParentName(station.getName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    @GetMapping("/listTodoItems")
    public TableDataInfo listTodoItems(SyTodoItems item){
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<String> roleCollect = user.getRoles().stream().map(role -> role.getRoleKey()).collect(Collectors.toList());
        List<SyTodoItems> list = null;
        if(SecurityUtils.getLoginUser().getUser().isAdmin()||roleCollect.contains("manager")){
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList1(item);
        } else {
            item.setFollowupId(user.getUserId());
            startPage();
            list = syTodoItemsService.selectSyTodoItemsList1(item);
        }
        if(list.size()>0){
            for(SyTodoItems res:list){
                if(res.getItemType()==1){
                    SyProject project = projectService.selectSyProjectByUuid(res.getItemParentId());
                    if(project!=null){
                        res.setItemParentName(project.getName());
                    }
                }else if(res.getItemType()==2){
                    SyEngineer engineer = engineerService.selectSyEngineerByUuid(res.getItemParentId());
                    if(engineer!=null){
                        res.setItemParentName(engineer.getName());
                    }
                }else if(res.getItemType()==3){
                    SySubEngineer subEngineer = subEngineerService.selectSySubEngineerByUuid(res.getItemParentId());
                    if(subEngineer!=null){
                        res.setItemParentName(subEngineer.getName());
                    }
                }else if(res.getItemType()==4){
                    SyStation station = stationService.selectSyStationByUuid(res.getItemParentId());
                    if(station!=null){
                        res.setItemParentName(station.getName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    @PostMapping("/addItemsForm")
    public AjaxResult addItemsForm(@RequestBody SyTodoItemsFollowup form){
        SyTodoItems items = syTodoItemsService.selectSyTodoItemsByUuid(form.getItemId());
        items.setStatus(1);
        SysUser nextFollowup = userService.selectUserById(form.getNextFollowupId());
        if(nextFollowup!=null){
            items.setFollowupName(nextFollowup.getNickName());
        }
        items.setFollowupId(form.getNextFollowupId());
        syTodoItemsService.updateSyTodoItems(items);
        form.setUuid(UUID.randomUUID().toString());
        SysUser followUp = userService.selectUserById(form.getFollowupId());
        if(followUp!=null){
            form.setFollowupPerson(followUp.getNickName());
        }
        form.setCreateTime(new Date());
        return toAjax(followupService.insertSyTodoItemsFollowup(form));
    }

    @GetMapping("/getItemListByItemParentId")
    public AjaxResult getItemListByItemParentId(SyTodoItems items){
        List<SyTodoItems> list = syTodoItemsService.selectSyTodoItemsList(items);
        return AjaxResult.success(list);
    }
}
