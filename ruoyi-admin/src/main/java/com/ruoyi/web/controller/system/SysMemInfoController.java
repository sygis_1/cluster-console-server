package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysMemInfo;
import com.ruoyi.system.service.ISysMemInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 内存信息表Controller
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/system/memory")
public class SysMemInfoController extends BaseController {

    @Autowired
    private ISysMemInfoService sysMemInfoService;

    /**
     * 查询内存信息表列表
     */
    @PreAuthorize("@ss.hasPermi('system:memory:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysMemInfo sysMemInfo)
    {
        startPage();
        List<SysMemInfo> list = sysMemInfoService.selectSysMemInfoList(sysMemInfo);
        return getDataTable(list);
    }

    /**
     * 导出内存信息表列表
     */
    @PreAuthorize("@ss.hasPermi('system:memory:export')")
    @Log(title = "内存信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysMemInfo sysMemInfo)
    {
        List<SysMemInfo> list = sysMemInfoService.selectSysMemInfoList(sysMemInfo);
        ExcelUtil<SysMemInfo> util = new ExcelUtil<SysMemInfo>(SysMemInfo.class);
        util.exportExcel(response, list, "内存信息表数据");
    }

    /**
     * 获取内存信息表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:memory:query')")
    @GetMapping(value = "/{serverIp}")
    public AjaxResult getInfo(@PathVariable("serverIp") String serverIp)
    {
        return success(sysMemInfoService.selectSysMemInfoByServerIp(serverIp));
    }

    /**
     * 新增内存信息表
     */
    @PreAuthorize("@ss.hasPermi('system:memory:add')")
    @Log(title = "内存信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysMemInfo sysMemInfo)
    {
        return toAjax(sysMemInfoService.insertSysMemInfo(sysMemInfo));
    }

    /**
     * 修改内存信息表
     */
    @PreAuthorize("@ss.hasPermi('system:memory:edit')")
    @Log(title = "内存信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysMemInfo sysMemInfo)
    {
        return toAjax(sysMemInfoService.updateSysMemInfo(sysMemInfo));
    }

    /**
     * 删除内存信息表
     */
    @PreAuthorize("@ss.hasPermi('system:memory:remove')")
    @Log(title = "内存信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{serverIps}")
    public AjaxResult remove(@PathVariable String[] serverIps)
    {
        return toAjax(sysMemInfoService.deleteSysMemInfoByServerIps(serverIps));
    }
}
