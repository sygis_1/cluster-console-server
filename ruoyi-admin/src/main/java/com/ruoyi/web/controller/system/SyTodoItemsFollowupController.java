package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyTodoItemsFollowup;
import com.ruoyi.system.service.ISyTodoItemsFollowupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 待办事项跟进记录Controller
 * 
 * @author Sy
 * @date 2023-10-08
 */
@RestController
@RequestMapping("/system/followup")
public class SyTodoItemsFollowupController extends BaseController {

    @Autowired
    private ISyTodoItemsFollowupService syTodoItemsFollowupService;

    /**
     * 查询待办事项跟进记录列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyTodoItemsFollowup syTodoItemsFollowup) {
        startPage();
        List<SyTodoItemsFollowup> list = syTodoItemsFollowupService.selectSyTodoItemsFollowupList(syTodoItemsFollowup);
        return getDataTable(list);
    }

    /**
     * 导出待办事项跟进记录列表
     */
    @Log(title = "待办事项跟进记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyTodoItemsFollowup syTodoItemsFollowup) {
        List<SyTodoItemsFollowup> list = syTodoItemsFollowupService.selectSyTodoItemsFollowupList(syTodoItemsFollowup);
        ExcelUtil<SyTodoItemsFollowup> util = new ExcelUtil<SyTodoItemsFollowup>(SyTodoItemsFollowup.class);
        util.exportExcel(response, list, "待办事项跟进记录数据");
    }

    /**
     * 获取待办事项跟进记录详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(syTodoItemsFollowupService.selectSyTodoItemsFollowupByUuid(uuid));
    }

    /**
     * 新增待办事项跟进记录
     */
    @Log(title = "待办事项跟进记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyTodoItemsFollowup syTodoItemsFollowup) {
        return toAjax(syTodoItemsFollowupService.insertSyTodoItemsFollowup(syTodoItemsFollowup));
    }

    /**
     * 修改待办事项跟进记录
     */
    @Log(title = "待办事项跟进记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyTodoItemsFollowup syTodoItemsFollowup) {
        return toAjax(syTodoItemsFollowupService.updateSyTodoItemsFollowup(syTodoItemsFollowup));
    }

    /**
     * 删除待办事项跟进记录
     */
    @Log(title = "待办事项跟进记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syTodoItemsFollowupService.deleteSyTodoItemsFollowupByUuids(uuids));
    }
}
