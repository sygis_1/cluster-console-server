package com.ruoyi.web.controller.system;

import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SyObjectInfo;
import com.ruoyi.system.service.ISyObjectInfoService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.utils.MinioFileUtil;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyDatasetInfo;
import com.ruoyi.system.service.ISyDatasetInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 数据集Controller
 * 
 * @author Sy
 * @date 2023-09-26
 */
@RestController
@RequestMapping("/system/dataset")
public class SyDatasetInfoController extends BaseController {

    @Autowired
    private ISyDatasetInfoService syDatasetInfoService;

    @Autowired
    private MinioFileUtil minioFileUtil;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISyObjectInfoService objectInfoService;

    @GetMapping("/listAllDatasets")
    public AjaxResult listAllDatasets(){
        List<SyDatasetInfo> list = syDatasetInfoService.selectSyDatasetInfoList(new SyDatasetInfo());
        return AjaxResult.success(list);
    }


    @PostMapping("/editDataset")
    public AjaxResult editDataset(@RequestBody SyDatasetInfo dataset) throws Exception{
        if(dataset.getUuid()==null){ //创建数据集
            SyDatasetInfo datasetInfo = new SyDatasetInfo();
            datasetInfo.setCode(dataset.getCode());
            List<SyDatasetInfo> res = syDatasetInfoService.selectSyDatasetInfoList(datasetInfo);
            if(res.size()>0){
                return AjaxResult.error("数据集名称重复！");
            }else {
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                dataset.setUuid(uuid);
                dataset.setStatus(0);
                dataset.setCreateBy(SecurityUtils.getLoginUser().getUserId().toString());
                Date createTime = new Date();
                dataset.setCreateTime(createTime);
                syDatasetInfoService.insertSyDatasetInfo(dataset);
                // 创建minio 存储筒
                if(!minioFileUtil.checkBucketExist(dataset.getCode().toLowerCase())){
                    minioFileUtil.createBucket(dataset.getCode().toLowerCase());
                    if(minioFileUtil.checkBucketExist(dataset.getCode().toLowerCase())){
                        minioFileUtil.startBucketPolicy(dataset.getCode().toLowerCase());
                    }
                }
                return AjaxResult.success();
            }
        }else{ //修改数据集
            dataset.setUpdateBy(SecurityUtils.getLoginUser().getUserId().toString());
            dataset.setUpdateTime(new Date());
            syDatasetInfoService.updateSyDatasetInfo(dataset);
            return AjaxResult.success();
        }
    }

    @PostMapping("/getCode")
    public AjaxResult getCode(@RequestBody SyDatasetInfo datasetInfo){
        String code = getFirstSpell(datasetInfo.getName());
        Map<Object,Object> res = new HashMap<>();
        res.put("code",code);
        SyDatasetInfo param = new SyDatasetInfo();
        param.setCode(code);
        List<SyDatasetInfo> list = syDatasetInfoService.selectSyDatasetInfoList(param);
        if(list.size()>0){
            return AjaxResult.error("数据集名称重复");
        }else{
            return AjaxResult.success(res);
        }
    }

    public static String getFirstSpell(String chinese) {
        StringBuffer pybf = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 128) {
                try {
                    String[] temp = PinyinHelper.toHanyuPinyinStringArray(arr[i], defaultFormat);
                    if (temp != null) {
                        pybf.append(temp[0].charAt(0));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                pybf.append(arr[i]);
            }
        }
        return pybf.toString().replaceAll("\\W", "").trim().toUpperCase();
    }

    /**
     * 查询数据集列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyDatasetInfo syDatasetInfo) {
        startPage();
        List<SyDatasetInfo> list = syDatasetInfoService.selectSyDatasetInfoList(syDatasetInfo);
        return getDataTable(list);
    }

    @GetMapping("/listUploadDataset")
    public TableDataInfo listUploadDataset(SyDatasetInfo syDatasetInfo) {
        startPage();
        List<SyDatasetInfo> list = syDatasetInfoService.selectUploadDataList(syDatasetInfo);
        if(list.size()>0){
            for(SyDatasetInfo res:list){
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                if(res.getUpdateBy()!=null){
                    SysUser offshelf = userService.selectUserById(Long.parseLong(res.getUpdateBy()));
                    if(offshelf!=null){
                        res.setUpdator(offshelf.getNickName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    @GetMapping("/listPublishDataset")
    public TableDataInfo listPublishDataset(SyDatasetInfo syDatasetInfo) {
        startPage();
        List<SyDatasetInfo> list = syDatasetInfoService.selectPublishDataList(syDatasetInfo);
        if(list.size()>0){
            for(SyDatasetInfo res:list){
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                if(res.getUpdateBy()!=null){
                    SysUser offshelf = userService.selectUserById(Long.parseLong(res.getUpdateBy()));
                    if(offshelf!=null){
                        res.setUpdator(offshelf.getNickName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    @GetMapping("/listQueryDataset")
    public TableDataInfo listQueryDataset(SyDatasetInfo syDatasetInfo) {
        startPage();
        List<SyDatasetInfo> list = syDatasetInfoService.listQueryDataset(syDatasetInfo);
        if(list.size()>0){
            for(SyDatasetInfo res:list){
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                if(res.getUpdateBy()!=null){
                    SysUser offshelf = userService.selectUserById(Long.parseLong(res.getUpdateBy()));
                    if(offshelf!=null){
                        res.setUpdator(offshelf.getNickName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    @GetMapping("/listOffShelfDataset")
    public TableDataInfo listOffShelfDataset(SyDatasetInfo syDatasetInfo) {
        startPage();
        List<SyDatasetInfo> list = syDatasetInfoService.selectOffShelfDataList(syDatasetInfo);
        if(list.size()>0){
            for(SyDatasetInfo res:list){
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                if(res.getUpdateBy()!=null){
                    SysUser offshelf = userService.selectUserById(Long.parseLong(res.getUpdateBy()));
                    if(offshelf!=null){
                        res.setUpdator(offshelf.getNickName());
                    }
                }
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出数据集列表
     */
    @Log(title = "数据集", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyDatasetInfo syDatasetInfo) {
        List<SyDatasetInfo> list = syDatasetInfoService.selectSyDatasetInfoList(syDatasetInfo);
        ExcelUtil<SyDatasetInfo> util = new ExcelUtil<SyDatasetInfo>(SyDatasetInfo.class);
        util.exportExcel(response, list, "数据集数据");
    }

    /**
     * 获取数据集详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        return success(syDatasetInfoService.selectSyDatasetInfoByUuid(uuid));
    }

    /**
     * 新增数据集
     */
    @Log(title = "数据集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyDatasetInfo syDatasetInfo) {
        return toAjax(syDatasetInfoService.insertSyDatasetInfo(syDatasetInfo));
    }

    /**
     * 修改数据集
     */
    @Log(title = "数据集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyDatasetInfo syDatasetInfo) {
        return toAjax(syDatasetInfoService.updateSyDatasetInfo(syDatasetInfo));
    }

    @PostMapping("/processPublishDataset")
    public AjaxResult processPublishDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            dataset.setStatus(1); // 发布成功
            dataset.setPublishId(SecurityUtils.getLoginUser().getUserId());
            dataset.setPublishTime(new Date());
            dataset.setUpdateTime(null);
            dataset.setUpdateBy(null);
            syDatasetInfoService.publishSyDatasetInfo(dataset);
            return AjaxResult.success();
        }else{
            return AjaxResult.error("对应数据集不存在！");
        }
    }

    @PostMapping("/processOffShelfDataset")
    public AjaxResult processOffShelfDataset(@RequestBody SyDatasetInfo param) throws Exception {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            dataset.setStatus(2); // 下架成功
            dataset.setUpdateBy(SecurityUtils.getLoginUser().getUserId().toString());
            dataset.setUpdateTime(new Date());
            syDatasetInfoService.updateSyDatasetInfo(dataset);
//            if(minioFileUtil.checkBucketExist(dataset.getCode().toLowerCase())){
//                minioFileUtil.stopBucketPolicy(dataset.getCode().toLowerCase());
//            }
            return AjaxResult.success();
        }else{
            return AjaxResult.error("对应数据集不存在！");
        }
    }

    @PostMapping("/processDeleteDataset")
    public AjaxResult processDeleteDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            syDatasetInfoService.deleteSyDatasetInfoByUuid(param.getUuid());
            return AjaxResult.success();
        }else{
            return AjaxResult.error("对应数据集不存在！");
        }
    }

    /**
     * 删除数据集
     */
    @Log(title = "数据集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syDatasetInfoService.deleteSyDatasetInfoByUuids(uuids));
    }

    @PostMapping("/uploadFiles")
    public AjaxResult uploadFiles(MultipartFile[] files,String bucket,String uuid) throws Exception{
        bucket = bucket.toLowerCase();
        for (MultipartFile file:files){
            String res = minioFileUtil.uploadFilesByStream(file,bucket);
            // 写入数据库
            SyObjectInfo object = new SyObjectInfo();
            object.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
            String name = file.getOriginalFilename().replaceAll(".glb","").replaceAll(".gltf","");
            object.setName(name);
            object.setDatasetId(uuid);
            object.setCreateBy("1");
            object.setCreateTime(new Date());
            object.setPath(res);
            object.setCaption(file.getOriginalFilename());
            objectInfoService.insertSyObjectInfo(object);
        }
        return AjaxResult.success();
    }

    @GetMapping("/getStationDatasets")
    public AjaxResult getStationDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> unbindStationList = syDatasetInfoService.selectUnbindStation(param.getStationIds());
        List<SyDatasetInfo> bindStationList = syDatasetInfoService.selectBindStation(param.getStationIds());
        if(unbindStationList.size()>0){
            for (SyDatasetInfo res:unbindStationList){
                res.setHasProject(0);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        if(bindStationList.size()>0){
            for (SyDatasetInfo res:bindStationList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getProjectDatasets")
    public AjaxResult getProjectDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> unbindProjectList = syDatasetInfoService.selectUnbindProject(param.getProjectIds());
        List<SyDatasetInfo> bindProjectList = syDatasetInfoService.selectBindProject(param.getProjectIds());
        if(unbindProjectList.size()>0){
            for (SyDatasetInfo res:unbindProjectList){
                res.setHasProject(0);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        if(bindProjectList.size()>0){
            for (SyDatasetInfo res:bindProjectList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getEngineerDatasets")
    public AjaxResult getEngineerDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> unbindEngineerList = syDatasetInfoService.selectUnbindEngineer(param.getEngineerIds());
        List<SyDatasetInfo> bindEngineerList = syDatasetInfoService.selectBindEngineer(param.getEngineerIds());
        if(unbindEngineerList.size()>0){
            for (SyDatasetInfo res:unbindEngineerList){
                res.setHasProject(0);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        if(bindEngineerList.size()>0){
            for (SyDatasetInfo res:bindEngineerList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getSubEngineerDatasets")
    public AjaxResult getSubEngineerDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> unbindSubEngineerList = syDatasetInfoService.selectUnbindSubEngineer(param.getSubEngineerIds());
        List<SyDatasetInfo> bindSubEngineerList = syDatasetInfoService.selectBindSubEngineer(param.getSubEngineerIds());
        if(unbindSubEngineerList.size()>0){
            for (SyDatasetInfo res:unbindSubEngineerList){
                res.setHasProject(0);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        if(bindSubEngineerList.size()>0){
            for (SyDatasetInfo res:bindSubEngineerList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getBindProjectDatasets")
    public AjaxResult getBindProjectDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> bindProjectList = syDatasetInfoService.selectBindProject(param.getProjectIds());
        if(bindProjectList.size()>0){
            for (SyDatasetInfo res:bindProjectList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getBindStationDatasets")
    public AjaxResult getBindStationDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> bindStationList = syDatasetInfoService.selectBindStation(param.getStationIds());
        if(bindStationList.size()>0){
            for (SyDatasetInfo res:bindStationList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getBindEngineerDatasets")
    public AjaxResult getBindEngineerDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> bindEngineerList = syDatasetInfoService.selectBindEngineer(param.getEngineerIds());
        if(bindEngineerList.size()>0){
            for (SyDatasetInfo res:bindEngineerList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @GetMapping("/getBindSubEngineerDatasets")
    public AjaxResult getBindSubEngineerDatasets(SyDatasetInfo param) {
        List<SyDatasetInfo> list = new ArrayList<>();
        List<SyDatasetInfo> bindSubEngineerList = syDatasetInfoService.selectBindSubEngineer(param.getSubEngineerIds());
        if(bindSubEngineerList.size()>0){
            for (SyDatasetInfo res:bindSubEngineerList){
                res.setHasProject(1);
                if(res.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(res.getCreateBy()));
                    if(user!=null){
                        res.setCreator(user.getNickName());
                    }
                }
                if(res.getPublishId()!=null){
                    SysUser publish = userService.selectUserById(res.getPublishId());
                    if (publish!=null){
                        res.setPublisher(publish.getNickName());
                    }
                }
                list.add(res);
            }
        }
        return AjaxResult.success(list);
    }

    @PostMapping("/processProjectDatasets")
    public AjaxResult processProjectDatasets(@RequestBody SyDatasetInfo param) {
        String[] ids = param.getIds().split(",");
        if(ids.length>0){
            for(String id:ids){
                SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(id);
                if(dataset!=null){
                    if(dataset.getProjectIds()!=null){
                        if(dataset.getProjectIds().indexOf(param.getProjectIds())<0){
                            dataset.setProjectIds(dataset.getProjectIds()+","+param.getProjectIds());
                        }
                    }else{
                        dataset.setProjectIds(param.getProjectIds());
                    }
                    syDatasetInfoService.updateSyDatasetInfo(dataset);
                }
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/processStationDatasets")
    public AjaxResult processStationDatasets(@RequestBody SyDatasetInfo param) {
        String[] ids = param.getIds().split(",");
        if(ids.length>0){
            for(String id:ids){
                SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(id);
                if(dataset!=null){
                    if(dataset.getStationIds()!=null){
                        if(dataset.getStationIds().indexOf(param.getStationIds())<0){
                            dataset.setStationIds(dataset.getStationIds()+","+param.getStationIds());
                        }
                    }else{
                        dataset.setStationIds(param.getStationIds());
                    }
                    syDatasetInfoService.updateSyDatasetInfo(dataset);
                }
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/processEngineerDatasets")
    public AjaxResult processEngineerDatasets(@RequestBody SyDatasetInfo param) {
        String[] ids = param.getIds().split(",");
        if(ids.length>0){
            for(String id:ids){
                SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(id);
                if(dataset!=null){
                    if(dataset.getEngineerIds()!=null){
                        if(dataset.getEngineerIds().indexOf(param.getEngineerIds())<0){
                            dataset.setEngineerIds(dataset.getEngineerIds()+","+param.getEngineerIds());
                        }
                    }else{
                        dataset.setEngineerIds(param.getEngineerIds());
                    }
                    syDatasetInfoService.updateSyDatasetInfo(dataset);
                }
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/processSubEngineerDatasets")
    public AjaxResult processSubEngineerDatasets(@RequestBody SyDatasetInfo param) {
        String[] ids = param.getIds().split(",");
        if(ids.length>0){
            for(String id:ids){
                SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(id);
                if(dataset!=null){
                    if(dataset.getSubEngineerIds()!=null){
                        if(dataset.getSubEngineerIds().indexOf(param.getSubEngineerIds())<0){
                            dataset.setSubEngineerIds(dataset.getSubEngineerIds()+","+param.getSubEngineerIds());
                        }
                    }else{
                        dataset.setSubEngineerIds(param.getSubEngineerIds());
                    }
                    syDatasetInfoService.updateSyDatasetInfo(dataset);
                }
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/unhookStationDataset")
    public AjaxResult unhookStationDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            if(dataset.getStationIds()!=null){
                String stationIds = dataset.getStationIds().replaceAll(param.getStationIds(),"").replaceAll(","+param.getStationIds(),"");
                dataset.setStationIds(stationIds);
                syDatasetInfoService.updateSyDatasetInfo(dataset);
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/unhookProjectDataset")
    public AjaxResult unhookProjectDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            if(dataset.getProjectIds()!=null){
                String projectIds = dataset.getProjectIds().replaceAll(param.getProjectIds(),"").replaceAll(","+param.getProjectIds(),"");
                dataset.setProjectIds(projectIds);
                syDatasetInfoService.updateSyDatasetInfo(dataset);
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/unhookEngineerDataset")
    public AjaxResult unhookEngineerDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            if(dataset.getEngineerIds()!=null){
                String engineerIds = dataset.getEngineerIds().replaceAll(param.getEngineerIds(),"").replaceAll(","+param.getEngineerIds(),"");
                dataset.setEngineerIds(engineerIds);
                syDatasetInfoService.updateSyDatasetInfo(dataset);
            }
        }
        return AjaxResult.success();
    }

    @PostMapping("/unhookSubEngineerDataset")
    public AjaxResult unhookSubEngineerDataset(@RequestBody SyDatasetInfo param) {
        SyDatasetInfo dataset = syDatasetInfoService.selectSyDatasetInfoByUuid(param.getUuid());
        if(dataset!=null){
            if(dataset.getSubEngineerIds()!=null){
                String subEngineerIds = dataset.getSubEngineerIds().replaceAll(param.getSubEngineerIds(),"").replaceAll(","+param.getSubEngineerIds(),"");
                dataset.setSubEngineerIds(subEngineerIds);
                syDatasetInfoService.updateSyDatasetInfo(dataset);
            }
        }
        return AjaxResult.success();
    }
}
