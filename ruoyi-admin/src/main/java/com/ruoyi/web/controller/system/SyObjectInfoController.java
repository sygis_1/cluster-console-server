package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.SyDatasetInfo;
import com.ruoyi.system.service.ISyDatasetInfoService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.web.controller.utils.MinioFileUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SyObjectInfo;
import com.ruoyi.system.service.ISyObjectInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据对象Controller
 * 
 * @author Sy
 * @date 2023-09-26
 */
@RestController
@RequestMapping("/system/objectinfo")
public class SyObjectInfoController extends BaseController {

    @Autowired
    private ISyObjectInfoService syObjectInfoService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private MinioFileUtil minioFileUtil;

    @Autowired
    private ISyDatasetInfoService datasetInfoService;

    /**
     * 查询数据对象列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SyObjectInfo syObjectInfo) {
        startPage();
        List<SyObjectInfo> list = syObjectInfoService.selectSyObjectInfoList(syObjectInfo);
        return getDataTable(list);
    }

    @GetMapping("/getDatasetObjectsList")
    public AjaxResult getDatasetObjectsList(SyObjectInfo syObjectInfo) {
        List<SyObjectInfo> list = syObjectInfoService.selectSyObjectInfoList(syObjectInfo);
        if(list.size()>0){
            for (SyObjectInfo object:list){
                if(object.getCreateBy()!=null){
                    SysUser user = userService.selectUserById(Long.parseLong(object.getCreateBy()));
                    if(user!=null){
                        object.setCreator(user.getNickName());
                    }
                }
            }
        }
        return AjaxResult.success(list);
    }

    /**
     * 导出数据对象列表
     */
    @Log(title = "数据对象", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SyObjectInfo syObjectInfo) {
        List<SyObjectInfo> list = syObjectInfoService.selectSyObjectInfoList(syObjectInfo);
        ExcelUtil<SyObjectInfo> util = new ExcelUtil<SyObjectInfo>(SyObjectInfo.class);
        util.exportExcel(response, list, "数据对象数据");
    }

    /**
     * 获取数据对象详细信息
     */
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid) {
        SyObjectInfo object = syObjectInfoService.selectSyObjectInfoByUuid(uuid);
        if(object!=null){
            SyDatasetInfo dataset = datasetInfoService.selectSyDatasetInfoByUuid(object.getDatasetId());
            object.setDatasetName(dataset.getName());
            object.setDatasetCode(dataset.getCode());
        }
        return AjaxResult.success(object);
    }

    /**
     * 新增数据对象
     */
    @Log(title = "数据对象", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SyObjectInfo syObjectInfo) {
        return toAjax(syObjectInfoService.insertSyObjectInfo(syObjectInfo));
    }

    /**
     * 修改数据对象
     */
    @Log(title = "数据对象", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SyObjectInfo syObjectInfo) {
        return toAjax(syObjectInfoService.updateSyObjectInfo(syObjectInfo));
    }

    @PostMapping("/deleteObjectInfo")
    public AjaxResult deleteObjectInfo(@RequestBody SyObjectInfo syObjectInfo) throws Exception {
        SyObjectInfo object = syObjectInfoService.selectSyObjectInfoByUuid(syObjectInfo.getUuid());
        if(object!=null){
            SyDatasetInfo dataset = datasetInfoService.selectSyDatasetInfoByUuid(object.getDatasetId());
            if(dataset!=null){
                minioFileUtil.removeBuckectObject(dataset.getCode().toLowerCase(),object.getCaption());
            }
        }
        syObjectInfoService.deleteSyObjectInfoByUuid(syObjectInfo.getUuid());
        return AjaxResult.success();
    }

    /**
     * 删除数据对象
     */
    @Log(title = "数据对象", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids) {
        return toAjax(syObjectInfoService.deleteSyObjectInfoByUuids(uuids));
    }
}
