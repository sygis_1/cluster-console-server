import request from '@/utils/request'

export function getCode(data) {
  return request({
    url: '/system/dataset/getCode',
    method: 'post',
    data: data
  })
}

export function editDataset(data) {
  return request({
    url: '/system/dataset/editDataset',
    method: 'post',
    data: data
  })
}

export function listUploadDataset(query) {
  return request({
    url: '/system/dataset/listUploadDataset',
    method: 'get',
    params: query
  })
}

export function listPublishDataset(query) {
  return request({
    url: '/system/dataset/listPublishDataset',
    method: 'get',
    params: query
  })
}

export function listQueryDataset(query) {
  return request({
    url: '/system/dataset/listQueryDataset',
    method: 'get',
    params: query
  })
}

export function listAllDatasets() {
  return request({
    url: '/system/dataset/listAllDatasets',
    method: 'get'
  })
}

export function listOffShelfDataset(query) {
  return request({
    url: '/system/dataset/listOffShelfDataset',
    method: 'get',
    params: query
  })
}

// 查询数据集列表
export function listDataset(query) {
  return request({
    url: '/system/dataset/list',
    method: 'get',
    params: query
  })
}

export function getStationDatasets(query) {
  return request({
    url: '/system/dataset/getStationDatasets',
    method: 'get',
    params: query
  })
}

export function getProjectDatasets(query) {
  return request({
    url: '/system/dataset/getProjectDatasets',
    method: 'get',
    params: query
  })
}

export function getEngineerDatasets(query) {
  return request({
    url: '/system/dataset/getEngineerDatasets',
    method: 'get',
    params: query
  })
}

export function getSubEngineerDatasets(query) {
  return request({
    url: '/system/dataset/getSubEngineerDatasets',
    method: 'get',
    params: query
  })
}

export function getBindProjectDatasets(query) {
  return request({
    url: '/system/dataset/getBindProjectDatasets',
    method: 'get',
    params: query
  })
}

export function getBindStationDatasets(query) {
  return request({
    url: '/system/dataset/getBindStationDatasets',
    method: 'get',
    params: query
  })
}

export function getBindEngineerDatasets(query) {
  return request({
    url: '/system/dataset/getBindEngineerDatasets',
    method: 'get',
    params: query
  })
}

export function getBindSubEngineerDatasets(query) {
  return request({
    url: '/system/dataset/getBindSubEngineerDatasets',
    method: 'get',
    params: query
  })
}

// 查询数据集详细
export function getDataset(uuid) {
  return request({
    url: '/system/dataset/' + uuid,
    method: 'get'
  })
}

// 新增数据集
export function addDataset(data) {
  return request({
    url: '/system/dataset',
    method: 'post',
    data: data
  })
}

export function processPublishDataset(data) {
  return request({
    url: '/system/dataset/processPublishDataset',
    method: 'post',
    data: data
  })
}

export function processOffShelfDataset(data) {
  return request({
    url: '/system/dataset/processOffShelfDataset',
    method: 'post',
    data: data
  })
}

export function unhookStationDataset(data) {
  return request({
    url: '/system/dataset/unhookStationDataset',
    method: 'post',
    data: data
  })
}

export function unhookProjectDataset(data) {
  return request({
    url: '/system/dataset/unhookProjectDataset',
    method: 'post',
    data: data
  })
}

export function unhookEngineerDataset(data) {
  return request({
    url: '/system/dataset/unhookEngineerDataset',
    method: 'post',
    data: data
  })
}

export function unhookSubEngineerDataset(data) {
  return request({
    url: '/system/dataset/unhookSubEngineerDataset',
    method: 'post',
    data: data
  })
}

export function processDeleteDataset(data) {
  return request({
    url: '/system/dataset/processDeleteDataset',
    method: 'post',
    data: data
  })
}

export function processProjectDatasets(data) {
  return request({
    url: '/system/dataset/processProjectDatasets',
    method: 'post',
    data: data
  })
}

export function processStationDatasets(data) {
  return request({
    url: '/system/dataset/processStationDatasets',
    method: 'post',
    data: data
  })
}

export function processEngineerDatasets(data) {
  return request({
    url: '/system/dataset/processEngineerDatasets',
    method: 'post',
    data: data
  })
}

export function processSubEngineerDatasets(data) {
  return request({
    url: '/system/dataset/processSubEngineerDatasets',
    method: 'post',
    data: data
  })
}


// 修改数据集
export function updateDataset(data) {
  return request({
    url: '/system/dataset',
    method: 'put',
    data: data
  })
}

// 删除数据集
export function delDataset(uuid) {
  return request({
    url: '/system/dataset/' + uuid,
    method: 'delete'
  })
}
