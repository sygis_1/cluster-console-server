import request from '@/utils/request'

// 查询待办事项列表
export function listItems(query) {
  return request({
    url: '/system/items/list',
    method: 'get',
    params: query
  })
}

export function listTodoItems(query) {
  return request({
    url: '/system/items/listTodoItems',
    method: 'get',
    params: query
  })
}

export function listDoneItems(query) {
  return request({
    url: '/system/items/listDoneItems',
    method: 'get',
    params: query
  })
}

export function listItemsForms(query) {
  return request({
    url: '/system/items/listItemsForms',
    method: 'get',
    params: query
  })
}

export function listItemComments(query) {
  return request({
    url: '/system/items/listItemComments',
    method: 'get',
    params: query
  })
}

// 查询待办事项详细
export function getItems(uuid) {
  return request({
    url: '/system/items/' + uuid,
    method: 'get'
  })
}

// 新增待办事项
export function addItems(data) {
  return request({
    url: '/system/items',
    method: 'post',
    data: data
  })
}

export function addItemsForm(data) {
  return request({
    url: '/system/items/addItemsForm',
    method: 'post',
    data: data
  })
}

export function addRecout(data) {
  return request({
    url: '/system/items/addRecout',
    method: 'post',
    data: data
  })
}

export function addAnnotion(data) {
  return request({
    url: '/system/items/addAnnotion',
    method: 'post',
    data: data
  })
}

// 修改待办事项
export function updateItems(data) {
  return request({
    url: '/system/items',
    method: 'put',
    data: data
  })
}

// 删除待办事项
export function delItems(uuid) {
  return request({
    url: '/system/items/' + uuid,
    method: 'delete'
  })
}


export function getitemParentInfoList(query) {
  return request({
    url: '/system/items/getitemParentInfoList',
    method: 'get',
    params: query
  })
}

export function editItem(data) {
  return request({
    url: '/system/items/editItem',
    method: 'post',
    data: data
  })
}

export function getItemListByItemParentId(query) {
  return request({
    url: '/system/items/getItemListByItemParentId',
    method: 'get',
    params: query
  })
}