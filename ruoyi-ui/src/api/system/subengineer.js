import request from '@/utils/request'

// 生成项目编号
export function createSubEngineerNo() {
    return request({
      url: '/system/subengineer/createSubEngineerNo',
      method: 'post'
    })
}

export function editSubEngineer(data) {
    return request({
      url: '/system/subengineer/editSubEngineer',
      method: 'post',
      data: data
    })
}

export function listSubEngineer(query) {
  return request({
    url: '/system/subengineer/list',
    method: 'get',
    params: query
  })
}

export function getSubEngineerList4Engineer(query) {
  return request({
    url: '/system/subengineer/getSubEngineerList4Engineer',
    method: 'get',
    params: query
  })
}

export function getSubEngineer4ProjectDetail(query) {
  return request({
    url: '/system/subengineer/getSubEngineer4ProjectDetail',
    method: 'get',
    params: query
  })
}


export function getSubEngineer(uuid) {
    return request({
      url: '/system/subengineer/' + uuid,
      method: 'get'
    })
}


export function processStartSubEngineer(data) {
    return request({
      url: '/system/subengineer/processStartSubEngineer',
      method: 'post',
      data: data
    })
}

export function processFinishSubEngineer(data) {
    return request({
      url: '/system/subengineer/processFinishSubEngineer',
      method: 'post',
      data: data
    })
}

export function processNullifySubEngineer(data) {
    return request({
      url: '/system/subengineer/processNullifySubEngineer',
      method: 'post',
      data: data
    })
}

export function processRestartSubEngineer(data) {
    return request({
      url: '/system/subengineer/processRestartSubEngineer',
      method: 'post',
      data: data
    })
}

export function processDeleteSubEngineer(data) {
    return request({
      url: '/system/subengineer/processDeleteSubEngineer',
      method: 'post',
      data: data
    })
}

export function processInitSubEngineer(data) {
    return request({
      url: '/system/subengineer/processInitSubEngineer',
      method: 'post',
      data: data
    })
}

export function unhookSubEngineer(data) {
  return request({
    url: '/system/subengineer/unhookSubEngineer',
    method: 'post',
    data: data
  })
}

export function getRoleSubEngineerList(query) {
  return request({
    url: '/system/subengineer/getRoleSubEngineerList',
    method: 'get',
    params: query
  })
}

export function saveRoleSubEngineer(data) {
  return request({
    url: '/system/subengineer/saveRoleSubEngineer',
    method: 'post',
    data: data
  })
}

export function unhookRoleSubEngineer(data) {
  return request({
    url: '/system/subengineer/unhookRoleSubEngineer',
    method: 'post',
    data: data
  })
}

export function getBindRoleSubEngineerList(query) {
  return request({
    url: '/system/subengineer/getBindRoleSubEngineerList',
    method: 'get',
    params: query
  })
}

export function getDatasetSubEngineerList(query) {
  return request({
    url: '/system/subengineer/getDatasetSubEngineerList',
    method: 'get',
    params: query
  })
}

export function saveDatasetSubEngineer(data) {
  return request({
    url: '/system/subengineer/saveDatasetSubEngineer',
    method: 'post',
    data: data
  })
}

export function unhookDatasetSubEngineer(data) {
  return request({
    url: '/system/subengineer/unhookDatasetSubEngineer',
    method: 'post',
    data: data
  })
}

export function getBindDatasetSubEngineerList(query) {
  return request({
    url: '/system/subengineer/getBindDatasetSubEngineerList',
    method: 'get',
    params: query
  })
}