import request from '@/utils/request'

export function getTreeRootData() {
    return request({
      url: '/system/gismap/getTreeRootData',
      method: 'get'
    })
  }