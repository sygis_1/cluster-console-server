import request from '@/utils/request'

// 查询引擎信息表列表
export function listEngine(query) {
  return request({
    url: '/system/engine/list',
    method: 'get',
    params: query
  })
}

// 查询引擎信息表详细
export function getEngine(serverIp) {
  return request({
    url: '/system/engine/' + serverIp,
    method: 'get'
  })
}

export function getClientInfo(query) {
  return request({
    url: '/system/engine/getClientInfo',
    method: 'get',
    params: query
  })
}

// 新增引擎信息表
export function addEngine(data) {
  return request({
    url: '/system/engine/addNewEngine',
    method: 'post',
    data: data
  })
}

// 修改引擎信息表
export function updateEngine(data) {
  return request({
    url: '/system/engine',
    method: 'put',
    data: data
  })
}

// 删除引擎信息表
export function delEngine(serverIp) {
  return request({
    url: '/system/engine/' + serverIp,
    method: 'delete'
  })
}
