import request from '@/utils/request'

// 生成项目编号
export function createEngineerNo() {
    return request({
      url: '/system/engineer/createEngineerNo',
      method: 'post'
    })
}

export function editEngineer(data) {
    return request({
      url: '/system/engineer/editEngineer',
      method: 'post',
      data: data
    })
}

export function listEngineer(query) {
    return request({
      url: '/system/engineer/list',
      method: 'get',
      params: query
    })
}

export function getProjectInfo4SubEngineer(query) {
    return request({
      url: '/system/engineer/getProjectInfo4SubEngineer',
      method: 'get',
      params: query
    })
}

export function getEngineer(uuid) {
    return request({
      url: '/system/engineer/' + uuid,
      method: 'get'
    })
}

export function getEngineerList() {
    return request({
      url: '/system/engineer/getEngineerList',
      method: 'get'
    })
}

export function getEngineerList4Project(query) {
  return request({
    url: '/system/engineer/getEngineerList4Project',
    method: 'get',
    params: query
  })
}

export function getEngineerList4ProjectDetail(query) {
  return request({
    url: '/system/engineer/getEngineerList4ProjectDetail',
    method: 'get',
    params: query
  })
}

export function unhookEngineer(data) {
  return request({
    url: '/system/engineer/unhookEngineer',
    method: 'post',
    data: data
  })
}

export function processStartEngineer(data) {
    return request({
      url: '/system/engineer/processStartEngineer',
      method: 'post',
      data: data
    })
}

export function processFinishEngineer(data) {
    return request({
      url: '/system/engineer/processFinishEngineer',
      method: 'post',
      data: data
    })
}

export function processNullifyEngineer(data) {
    return request({
      url: '/system/engineer/processNullifyEngineer',
      method: 'post',
      data: data
    })
}

export function processRestartEngineer(data) {
    return request({
      url: '/system/engineer/processRestartEngineer',
      method: 'post',
      data: data
    })
}

export function processDeleteEngineer(data) {
    return request({
      url: '/system/engineer/processDeleteEngineer',
      method: 'post',
      data: data
    })
}

export function processInitEngineer(data) {
    return request({
      url: '/system/engineer/processInitEngineer',
      method: 'post',
      data: data
    })
}

export function getRoleEngineerList(query) {
  return request({
    url: '/system/engineer/getRoleEngineerList',
    method: 'get',
    params: query
  })
}

export function saveRoleEngineer(data) {
  return request({
    url: '/system/engineer/saveRoleEngineer',
    method: 'post',
    data: data
  })
}

export function unhookRoleEngineer(data) {
  return request({
    url: '/system/engineer/unhookRoleEngineer',
    method: 'post',
    data: data
  })
}

export function getBindRoleEngineerList(query) {
  return request({
    url: '/system/engineer/getBindRoleEngineerList',
    method: 'get',
    params: query
  })
}

export function getDatasetEngineerList(query) {
  return request({
    url: '/system/engineer/getDatasetEngineerList',
    method: 'get',
    params: query
  })
}

export function saveDatasetEngineer(data) {
  return request({
    url: '/system/engineer/saveDatasetEngineer',
    method: 'post',
    data: data
  })
}

export function unhookDatasetEngineer(data) {
  return request({
    url: '/system/engineer/unhookDatasetEngineer',
    method: 'post',
    data: data
  })
}

export function getBindDatasetEngineerList(query) {
  return request({
    url: '/system/engineer/getBindDatasetEngineerList',
    method: 'get',
    params: query
  })
}