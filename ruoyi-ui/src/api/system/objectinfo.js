import request from '@/utils/request'

// 查询数据对象列表
export function listObjectinfo(query) {
  return request({
    url: '/system/objectinfo/list',
    method: 'get',
    params: query
  })
}

export function getDatasetObjectsList(query) {
  return request({
    url: '/system/objectinfo/getDatasetObjectsList',
    method: 'get',
    params: query
  })
}

// 查询数据对象详细
export function getObjectinfo(uuid) {
  return request({
    url: '/system/objectinfo/' + uuid,
    method: 'get'
  })
}

// 新增数据对象
export function addObjectinfo(data) {
  return request({
    url: '/system/objectinfo',
    method: 'post',
    data: data
  })
}

export function deleteObjectInfo(data) {
  return request({
    url: '/system/objectinfo/deleteObjectInfo',
    method: 'post',
    data: data
  })
}

// 修改数据对象
export function updateObjectinfo(data) {
  return request({
    url: '/system/objectinfo',
    method: 'put',
    data: data
  })
}

// 删除数据对象
export function delObjectinfo(uuid) {
  return request({
    url: '/system/objectinfo/' + uuid,
    method: 'delete'
  })
}
