import request from '@/utils/request'

export function listComponents(query) {
    return request({
      url: '/system/component/list',
      method: 'get',
      params: query
    })
}

export function importComponent(data) {
    return request({
      url: '/system/component/importComponent',
      method: 'post',
      data: data
    })
}

export function getComponentInfo(query) {
    return request({
      url: '/system/component/getComponentInfo',
      method: 'get',
      params: query
    })
}

export function updateComponentInfo(data) {
    return request({
      url: '/system/component/updateComponentInfo',
      method: 'post',
      data: data
    })
}