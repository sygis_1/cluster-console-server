import request from '@/utils/request'

// 生成项目编号
export function createProjectNo() {
    return request({
      url: '/system/project/createProjectNo',
      method: 'post'
    })
}

// 编辑项目信息
export function editProject(data) {
    return request({
      url: '/system/project/editProject',
      method: 'post',
      data: data
    })
}

// 查询项目列表
export function listProject(query) {
  return request({
    url: '/system/project/list',
    method: 'get',
    params: query
  })
}

// 查询项目列表
export function getRoleProjectList(query) {
  return request({
    url: '/system/project/getRoleProjectList',
    method: 'get',
    params: query
  })
}

export function getBindRoleProjectList(query) {
  return request({
    url: '/system/project/getBindRoleProjectList',
    method: 'get',
    params: query
  })
}

// 查询项目信息详细
export function getProject(uuid) {
  return request({
    url: '/system/project/' + uuid,
    method: 'get'
  })
}

// 编辑项目信息
export function processStartProject(data) {
    return request({
      url: '/system/project/processStartProject',
      method: 'post',
      data: data
    })
}

export function processFinishProject(data) {
    return request({
      url: '/system/project/processFinishProject',
      method: 'post',
      data: data
    })
}

export function processNullifyProject(data) {
    return request({
      url: '/system/project/processNullifyProject',
      method: 'post',
      data: data
    })
}

export function processDeleteProject(data) {
    return request({
      url: '/system/project/processDeleteProject',
      method: 'post',
      data: data
    })
}

export function processRestartProject(data) {
    return request({
      url: '/system/project/processRestartProject',
      method: 'post',
      data: data
    })
}

export function processInitProject(data) {
  return request({
    url: '/system/project/processInitProject',
    method: 'post',
    data: data
  })
}

// 查询项目列表
export function getProjectList() {
  return request({
    url: '/system/project/getProjectList',
    method: 'get'
  })
}

export function saveRoleProjects(data) {
  return request({
    url: '/system/project/saveRoleProjects',
    method: 'post',
    data: data
  })
}

export function unhookRoleProject(data) {
  return request({
    url: '/system/project/unhookRoleProject',
    method: 'post',
    data: data
  })
}

export function getDatasetProjectList(query) {
  return request({
    url: '/system/project/getDatasetProjectList',
    method: 'get',
    params: query
  })
}

export function saveDatasetProjects(data) {
  return request({
    url: '/system/project/saveDatasetProjects',
    method: 'post',
    data: data
  })
}

export function unhookDatasetProject(data) {
  return request({
    url: '/system/project/unhookDatasetProject',
    method: 'post',
    data: data
  })
}

export function getBindDatasetProjectList(query) {
  return request({
    url: '/system/project/getBindDatasetProjectList',
    method: 'get',
    params: query
  })
}