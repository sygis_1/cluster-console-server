import request from '@/utils/request'

export function editStation(data) {
    return request({
      url: '/system/station/editStation',
      method: 'post',
      data: data
    })
}


export function listStation(query) {
    return request({
      url: '/system/station/list',
      method: 'get',
      params: query
    })
}

export function getStation(uuid) {
    return request({
      url: '/system/station/' + uuid,
      method: 'get'
    })
}

export function processStartStation(data) {
    return request({
      url: '/system/station/processStartStation',
      method: 'post',
      data: data
    })
}

export function processFinishStation(data) {
    return request({
      url: '/system/station/processFinishStation',
      method: 'post',
      data: data
    })
}

export function processNullifyStation(data) {
    return request({
      url: '/system/station/processNullifyStation',
      method: 'post',
      data: data
    })
}

export function processDeleteStation(data) {
    return request({
      url: '/system/station/processDeleteStation',
      method: 'post',
      data: data
    })
}

export function processRestartStation(data) {
    return request({
      url: '/system/station/processRestartStation',
      method: 'post',
      data: data
    })
}

export function getRoleStationList(query) {
  return request({
    url: '/system/station/getRoleStationList',
    method: 'get',
    params: query
  })
}

export function saveRoleStation(data) {
  return request({
    url: '/system/station/saveRoleStation',
    method: 'post',
    data: data
  })
}

export function unhookRoleStation(data) {
  return request({
    url: '/system/station/unhookRoleStation',
    method: 'post',
    data: data
  })
}

export function getBindRoleStationList(query) {
  return request({
    url: '/system/station/getBindRoleStationList',
    method: 'get',
    params: query
  })
}

export function getDatasetStationList(query) {
  return request({
    url: '/system/station/getDatasetStationList',
    method: 'get',
    params: query
  })
}

export function saveDatasetStation(data) {
  return request({
    url: '/system/station/saveDatasetStation',
    method: 'post',
    data: data
  })
}

export function unhookDatasetStation(data) {
  return request({
    url: '/system/station/unhookDatasetStation',
    method: 'post',
    data: data
  })
}

export function getBindDatasetStationList(query) {
  return request({
    url: '/system/station/getBindDatasetStationList',
    method: 'get',
    params: query
  })
}