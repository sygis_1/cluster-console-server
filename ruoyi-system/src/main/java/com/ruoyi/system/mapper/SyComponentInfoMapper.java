package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyComponentInfo;

/**
 * 部件表Mapper接口
 * 
 * @author Sy
 * @date 2023-09-28
 */
public interface SyComponentInfoMapper 
{
    /**
     * 查询部件表
     * 
     * @param uuid 部件表主键
     * @return 部件表
     */
    public SyComponentInfo selectSyComponentInfoByUuid(String uuid);

    /**
     * 查询部件表列表
     * 
     * @param syComponentInfo 部件表
     * @return 部件表集合
     */
    public List<SyComponentInfo> selectSyComponentInfoList(SyComponentInfo syComponentInfo);

    /**
     * 新增部件表
     * 
     * @param syComponentInfo 部件表
     * @return 结果
     */
    public int insertSyComponentInfo(SyComponentInfo syComponentInfo);

    /**
     * 修改部件表
     * 
     * @param syComponentInfo 部件表
     * @return 结果
     */
    public int updateSyComponentInfo(SyComponentInfo syComponentInfo);

    /**
     * 删除部件表
     * 
     * @param uuid 部件表主键
     * @return 结果
     */
    public int deleteSyComponentInfoByUuid(String uuid);

    /**
     * 批量删除部件表
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyComponentInfoByUuids(String[] uuids);
}
