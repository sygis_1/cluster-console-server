package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysEngineInfo;

/**
 * 引擎信息表Mapper接口
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public interface SysEngineInfoMapper 
{
    /**
     * 查询引擎信息表
     * 
     * @param serverIp 引擎信息表主键
     * @return 引擎信息表
     */
    public SysEngineInfo selectSysEngineInfoByServerIp(String serverIp);

    /**
     * 查询引擎信息表列表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 引擎信息表集合
     */
    public List<SysEngineInfo> selectSysEngineInfoList(SysEngineInfo sysEngineInfo);

    /**
     * 新增引擎信息表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 结果
     */
    public int insertSysEngineInfo(SysEngineInfo sysEngineInfo);

    /**
     * 修改引擎信息表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 结果
     */
    public int updateSysEngineInfo(SysEngineInfo sysEngineInfo);

    /**
     * 删除引擎信息表
     * 
     * @param serverIp 引擎信息表主键
     * @return 结果
     */
    public int deleteSysEngineInfoByServerIp(String serverIp);

    /**
     * 批量删除引擎信息表
     * 
     * @param serverIps 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysEngineInfoByServerIps(String[] serverIps);
}
