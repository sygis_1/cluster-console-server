package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SyProject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 项目信息表Mapper接口
 * 
 * @author Sy
 * @date 2023-09-13
 */
public interface SyProjectMapper {

    /**
     * 查询项目信息表
     * 
     * @param uuid 项目信息表主键
     * @return 项目信息表
     */
    public SyProject selectSyProjectByUuid(String uuid);

    /**
     * 查询项目信息表列表
     * 
     * @param syProject 项目信息表
     * @return 项目信息表集合
     */
    public List<SyProject> selectSyProjectList(SyProject syProject);

    /**
     * 新增项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    public int insertSyProject(SyProject syProject);

    /**
     * 修改项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    public int updateSyProject(SyProject syProject);

    /**
     * 删除项目信息表
     * 
     * @param uuid 项目信息表主键
     * @return 结果
     */
    public int deleteSyProjectByUuid(String uuid);

    /**
     * 批量删除项目信息表
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyProjectByUuids(String[] uuids);

    public List<SyProject> selectProjectList4Select();

    public List<SyProject> selectUnbindRoleList(@Param("roleIds") String roleIds);

    public List<SyProject> selectBindRoleList(@Param("roleIds") String roleIds);

    public List<SyProject> selectDatasetProjectList();
}
