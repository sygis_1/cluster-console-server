package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysMemInfo;

/**
 * 内存信息表Mapper接口
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public interface SysMemInfoMapper 
{
    /**
     * 查询内存信息表
     * 
     * @param serverIp 内存信息表主键
     * @return 内存信息表
     */
    public SysMemInfo selectSysMemInfoByServerIp(String serverIp);

    /**
     * 查询内存信息表列表
     * 
     * @param sysMemInfo 内存信息表
     * @return 内存信息表集合
     */
    public List<SysMemInfo> selectSysMemInfoList(SysMemInfo sysMemInfo);

    /**
     * 新增内存信息表
     * 
     * @param sysMemInfo 内存信息表
     * @return 结果
     */
    public int insertSysMemInfo(SysMemInfo sysMemInfo);

    /**
     * 修改内存信息表
     * 
     * @param sysMemInfo 内存信息表
     * @return 结果
     */
    public int updateSysMemInfo(SysMemInfo sysMemInfo);

    /**
     * 删除内存信息表
     * 
     * @param serverIp 内存信息表主键
     * @return 结果
     */
    public int deleteSysMemInfoByServerIp(String serverIp);

    /**
     * 批量删除内存信息表
     * 
     * @param serverIps 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysMemInfoByServerIps(String[] serverIps);
}
