package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyTodoItemsComments;

/**
 * 待办事项追述Mapper接口
 * 
 * @author Sy
 * @date 2023-10-08
 */
public interface SyTodoItemsCommentsMapper 
{
    /**
     * 查询待办事项追述
     * 
     * @param uuid 待办事项追述主键
     * @return 待办事项追述
     */
    public SyTodoItemsComments selectSyTodoItemsCommentsByUuid(String uuid);

    /**
     * 查询待办事项追述列表
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 待办事项追述集合
     */
    public List<SyTodoItemsComments> selectSyTodoItemsCommentsList(SyTodoItemsComments syTodoItemsComments);

    /**
     * 新增待办事项追述
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 结果
     */
    public int insertSyTodoItemsComments(SyTodoItemsComments syTodoItemsComments);

    /**
     * 修改待办事项追述
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 结果
     */
    public int updateSyTodoItemsComments(SyTodoItemsComments syTodoItemsComments);

    /**
     * 删除待办事项追述
     * 
     * @param uuid 待办事项追述主键
     * @return 结果
     */
    public int deleteSyTodoItemsCommentsByUuid(String uuid);

    /**
     * 批量删除待办事项追述
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyTodoItemsCommentsByUuids(String[] uuids);
}
