package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysGpuInfo;

/**
 * GPU信息表Mapper接口
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public interface SysGpuInfoMapper 
{
    /**
     * 查询GPU信息表
     * 
     * @param serverIp GPU信息表主键
     * @return GPU信息表
     */
    public SysGpuInfo selectSysGpuInfoByServerIp(String serverIp);

    /**
     * 查询GPU信息表列表
     * 
     * @param sysGpuInfo GPU信息表
     * @return GPU信息表集合
     */
    public List<SysGpuInfo> selectSysGpuInfoList(SysGpuInfo sysGpuInfo);

    /**
     * 新增GPU信息表
     * 
     * @param sysGpuInfo GPU信息表
     * @return 结果
     */
    public int insertSysGpuInfo(SysGpuInfo sysGpuInfo);

    /**
     * 修改GPU信息表
     * 
     * @param sysGpuInfo GPU信息表
     * @return 结果
     */
    public int updateSysGpuInfo(SysGpuInfo sysGpuInfo);

    /**
     * 删除GPU信息表
     * 
     * @param serverIp GPU信息表主键
     * @return 结果
     */
    public int deleteSysGpuInfoByServerIp(String serverIp);

    /**
     * 批量删除GPU信息表
     * 
     * @param serverIps 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysGpuInfoByServerIps(String[] serverIps);
}
