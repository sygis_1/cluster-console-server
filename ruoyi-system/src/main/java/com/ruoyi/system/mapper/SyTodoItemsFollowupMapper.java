package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyTodoItemsFollowup;

/**
 * 待办事项跟进记录Mapper接口
 * 
 * @author Sy
 * @date 2023-10-08
 */
public interface SyTodoItemsFollowupMapper 
{
    /**
     * 查询待办事项跟进记录
     * 
     * @param uuid 待办事项跟进记录主键
     * @return 待办事项跟进记录
     */
    public SyTodoItemsFollowup selectSyTodoItemsFollowupByUuid(String uuid);

    /**
     * 查询待办事项跟进记录列表
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 待办事项跟进记录集合
     */
    public List<SyTodoItemsFollowup> selectSyTodoItemsFollowupList(SyTodoItemsFollowup syTodoItemsFollowup);

    /**
     * 新增待办事项跟进记录
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 结果
     */
    public int insertSyTodoItemsFollowup(SyTodoItemsFollowup syTodoItemsFollowup);

    /**
     * 修改待办事项跟进记录
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 结果
     */
    public int updateSyTodoItemsFollowup(SyTodoItemsFollowup syTodoItemsFollowup);

    /**
     * 删除待办事项跟进记录
     * 
     * @param uuid 待办事项跟进记录主键
     * @return 结果
     */
    public int deleteSyTodoItemsFollowupByUuid(String uuid);

    /**
     * 批量删除待办事项跟进记录
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyTodoItemsFollowupByUuids(String[] uuids);
}
