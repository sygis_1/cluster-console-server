package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SyStation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 站点信息表Mapper接口
 * 
 * @author Sy
 * @date 2023-09-13
 */
public interface SyStationMapper {

    /**
     * 查询站点信息表
     * 
     * @param uuid 站点信息表主键
     * @return 站点信息表
     */
    public SyStation selectSyStationByUuid(String uuid);

    /**
     * 查询站点信息表列表
     * 
     * @param syStation 站点信息表
     * @return 站点信息表集合
     */
    public List<SyStation> selectSyStationList(SyStation syStation);

    /**
     * 新增站点信息表
     * 
     * @param syStation 站点信息表
     * @return 结果
     */
    public int insertSyStation(SyStation syStation);

    /**
     * 修改站点信息表
     * 
     * @param syStation 站点信息表
     * @return 结果
     */
    public int updateSyStation(SyStation syStation);

    /**
     * 删除站点信息表
     * 
     * @param uuid 站点信息表主键
     * @return 结果
     */
    public int deleteSyStationByUuid(String uuid);

    /**
     * 批量删除站点信息表
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyStationByUuids(String[] uuids);

    public List<SyStation> selectUnbindRoleList(@Param("roleIds") String roleIds);

    public List<SyStation> selectBindRoleList(@Param("roleIds") String roleIds);

    public List<SyStation> selectDatasetStationList();
}
