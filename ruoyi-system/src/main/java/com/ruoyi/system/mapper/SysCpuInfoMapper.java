package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysCpuInfo;

/**
 * cpu信息Mapper接口
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public interface SysCpuInfoMapper 
{
    /**
     * 查询cpu信息
     * 
     * @param serverIp cpu信息主键
     * @return cpu信息
     */
    public SysCpuInfo selectSysCpuInfoByServerIp(String serverIp);

    /**
     * 查询cpu信息列表
     * 
     * @param sysCpuInfo cpu信息
     * @return cpu信息集合
     */
    public List<SysCpuInfo> selectSysCpuInfoList(SysCpuInfo sysCpuInfo);

    /**
     * 新增cpu信息
     * 
     * @param sysCpuInfo cpu信息
     * @return 结果
     */
    public int insertSysCpuInfo(SysCpuInfo sysCpuInfo);

    /**
     * 修改cpu信息
     * 
     * @param sysCpuInfo cpu信息
     * @return 结果
     */
    public int updateSysCpuInfo(SysCpuInfo sysCpuInfo);

    /**
     * 删除cpu信息
     * 
     * @param serverIp cpu信息主键
     * @return 结果
     */
    public int deleteSysCpuInfoByServerIp(String serverIp);

    /**
     * 批量删除cpu信息
     * 
     * @param serverIps 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysCpuInfoByServerIps(String[] serverIps);
}
