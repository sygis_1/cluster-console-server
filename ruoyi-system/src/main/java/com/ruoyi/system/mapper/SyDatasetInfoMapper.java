package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SyDatasetInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 数据集Mapper接口
 * 
 * @author Sy
 * @date 2023-09-26
 */
public interface SyDatasetInfoMapper {
    /**
     * 查询数据集
     * 
     * @param uuid 数据集主键
     * @return 数据集
     */
    public SyDatasetInfo selectSyDatasetInfoByUuid(String uuid);

    /**
     * 查询数据集列表
     * 
     * @param syDatasetInfo 数据集
     * @return 数据集集合
     */
    public List<SyDatasetInfo> selectSyDatasetInfoList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectUploadDataList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectPublishDataList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> listQueryDataset(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectOffShelfDataList(SyDatasetInfo syDatasetInfo);

    /**
     * 新增数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    public int insertSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    /**
     * 修改数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    public int updateSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    public int publishSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    /**
     * 删除数据集
     * 
     * @param uuid 数据集主键
     * @return 结果
     */
    public int deleteSyDatasetInfoByUuid(String uuid);

    /**
     * 批量删除数据集
     * 
     * @param uuids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSyDatasetInfoByUuids(String[] uuids);

    public List<SyDatasetInfo> selectUnbindStation(@Param("stationIds") String stationIds);

    public List<SyDatasetInfo> selectBindStation(@Param("stationIds") String stationIds);

    public List<SyDatasetInfo> selectUnbindProject(@Param("projectIds") String projectIds);

    public List<SyDatasetInfo> selectBindProject(@Param("projectIds") String projectIds);

    public List<SyDatasetInfo> selectUnbindEngineer(@Param("engineerIds") String engineerIds);

    public List<SyDatasetInfo> selectBindEngineer(@Param("engineerIds") String engineerIds);

    public List<SyDatasetInfo> selectUnbindSubEngineer(@Param("subEngineerIds") String subEngineerIds);

    public List<SyDatasetInfo> selectBindSubEngineer(@Param("subEngineerIds") String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndProjectId(@Param("uuid")String uuid,@Param("projectIds")String projectIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndEngineerId(@Param("uuid")String uuid,@Param("engineerIds")String engineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndSubEngineerId(@Param("uuid")String uuid,@Param("subEngineerIds")String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndStationId(@Param("uuid")String uuid,@Param("stationIds")String stationIds);

    public List<SyDatasetInfo> selectDatasetByProjectId(@Param("projectIds") String projectIds);

    public List<SyDatasetInfo> selectDatasetByEngineerId(@Param("engineerIds") String engineerIds);

    public List<SyDatasetInfo> selectDatasetBySubEngineerId(@Param("subEngineerIds") String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByStationId(@Param("stationIds") String stationIds);
}
