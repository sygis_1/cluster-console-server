package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyStation;
import com.ruoyi.system.mapper.SyStationMapper;
import com.ruoyi.system.service.ISyStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 站点信息表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-13
 */
@Service
public class SyStationServiceImpl implements ISyStationService {

    @Autowired
    private SyStationMapper syStationMapper;

    /**
     * 查询站点信息表
     * 
     * @param uuid 站点信息表主键
     * @return 站点信息表
     */
    @Override
    public SyStation selectSyStationByUuid(String uuid)
    {
        return syStationMapper.selectSyStationByUuid(uuid);
    }

    /**
     * 查询站点信息表列表
     * 
     * @param syStation 站点信息表
     * @return 站点信息表
     */
    @Override
    public List<SyStation> selectSyStationList(SyStation syStation)
    {
        return syStationMapper.selectSyStationList(syStation);
    }

    /**
     * 新增站点信息表
     * 
     * @param syStation 站点信息表
     * @return 结果
     */
    @Override
    public int insertSyStation(SyStation syStation)
    {
        syStation.setCreateTime(DateUtils.getNowDate());
        return syStationMapper.insertSyStation(syStation);
    }

    /**
     * 修改站点信息表
     * 
     * @param syStation 站点信息表
     * @return 结果
     */
    @Override
    public int updateSyStation(SyStation syStation)
    {
        syStation.setUpdateTime(DateUtils.getNowDate());
        return syStationMapper.updateSyStation(syStation);
    }

    /**
     * 批量删除站点信息表
     * 
     * @param uuids 需要删除的站点信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyStationByUuids(String[] uuids) {
        return syStationMapper.deleteSyStationByUuids(uuids);
    }

    /**
     * 删除站点信息表信息
     * 
     * @param uuid 站点信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyStationByUuid(String uuid) {
        return syStationMapper.deleteSyStationByUuid(uuid);
    }

    @Override
    public List<SyStation> selectUnbindRoleList(String roleIds){
        return syStationMapper.selectUnbindRoleList(roleIds);
    }

    @Override
    public List<SyStation> selectBindRoleList(String roleIds){
        return syStationMapper.selectBindRoleList(roleIds);
    }

    @Override
    public List<SyStation> selectDatasetStationList(){
        return syStationMapper.selectDatasetStationList();
    }
}
