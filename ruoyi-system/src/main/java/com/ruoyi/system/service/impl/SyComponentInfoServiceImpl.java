package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyComponentInfoMapper;
import com.ruoyi.system.domain.SyComponentInfo;
import com.ruoyi.system.service.ISyComponentInfoService;

/**
 * 部件表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-28
 */
@Service
public class SyComponentInfoServiceImpl implements ISyComponentInfoService 
{
    @Autowired
    private SyComponentInfoMapper syComponentInfoMapper;

    /**
     * 查询部件表
     * 
     * @param uuid 部件表主键
     * @return 部件表
     */
    @Override
    public SyComponentInfo selectSyComponentInfoByUuid(String uuid)
    {
        return syComponentInfoMapper.selectSyComponentInfoByUuid(uuid);
    }

    /**
     * 查询部件表列表
     * 
     * @param syComponentInfo 部件表
     * @return 部件表
     */
    @Override
    public List<SyComponentInfo> selectSyComponentInfoList(SyComponentInfo syComponentInfo)
    {
        return syComponentInfoMapper.selectSyComponentInfoList(syComponentInfo);
    }

    /**
     * 新增部件表
     * 
     * @param syComponentInfo 部件表
     * @return 结果
     */
    @Override
    public int insertSyComponentInfo(SyComponentInfo syComponentInfo)
    {
        syComponentInfo.setCreateTime(DateUtils.getNowDate());
        return syComponentInfoMapper.insertSyComponentInfo(syComponentInfo);
    }

    /**
     * 修改部件表
     * 
     * @param syComponentInfo 部件表
     * @return 结果
     */
    @Override
    public int updateSyComponentInfo(SyComponentInfo syComponentInfo)
    {
        syComponentInfo.setUpdateTime(DateUtils.getNowDate());
        return syComponentInfoMapper.updateSyComponentInfo(syComponentInfo);
    }

    /**
     * 批量删除部件表
     * 
     * @param uuids 需要删除的部件表主键
     * @return 结果
     */
    @Override
    public int deleteSyComponentInfoByUuids(String[] uuids)
    {
        return syComponentInfoMapper.deleteSyComponentInfoByUuids(uuids);
    }

    /**
     * 删除部件表信息
     * 
     * @param uuid 部件表主键
     * @return 结果
     */
    @Override
    public int deleteSyComponentInfoByUuid(String uuid)
    {
        return syComponentInfoMapper.deleteSyComponentInfoByUuid(uuid);
    }
}
