package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyObjectInfoMapper;
import com.ruoyi.system.domain.SyObjectInfo;
import com.ruoyi.system.service.ISyObjectInfoService;

/**
 * 数据对象Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-26
 */
@Service
public class SyObjectInfoServiceImpl implements ISyObjectInfoService 
{
    @Autowired
    private SyObjectInfoMapper syObjectInfoMapper;

    /**
     * 查询数据对象
     * 
     * @param uuid 数据对象主键
     * @return 数据对象
     */
    @Override
    public SyObjectInfo selectSyObjectInfoByUuid(String uuid) {
        return syObjectInfoMapper.selectSyObjectInfoByUuid(uuid);
    }

    /**
     * 查询数据对象列表
     * 
     * @param syObjectInfo 数据对象
     * @return 数据对象
     */
    @Override
    public List<SyObjectInfo> selectSyObjectInfoList(SyObjectInfo syObjectInfo) {
        return syObjectInfoMapper.selectSyObjectInfoList(syObjectInfo);
    }

    /**
     * 新增数据对象
     * 
     * @param syObjectInfo 数据对象
     * @return 结果
     */
    @Override
    public int insertSyObjectInfo(SyObjectInfo syObjectInfo)
    {
        syObjectInfo.setCreateTime(DateUtils.getNowDate());
        return syObjectInfoMapper.insertSyObjectInfo(syObjectInfo);
    }

    /**
     * 修改数据对象
     * 
     * @param syObjectInfo 数据对象
     * @return 结果
     */
    @Override
    public int updateSyObjectInfo(SyObjectInfo syObjectInfo) {
        syObjectInfo.setUpdateTime(DateUtils.getNowDate());
        return syObjectInfoMapper.updateSyObjectInfo(syObjectInfo);
    }

    /**
     * 批量删除数据对象
     * 
     * @param uuids 需要删除的数据对象主键
     * @return 结果
     */
    @Override
    public int deleteSyObjectInfoByUuids(String[] uuids)
    {
        return syObjectInfoMapper.deleteSyObjectInfoByUuids(uuids);
    }

    /**
     * 删除数据对象信息
     * 
     * @param uuid 数据对象主键
     * @return 结果
     */
    @Override
    public int deleteSyObjectInfoByUuid(String uuid)
    {
        return syObjectInfoMapper.deleteSyObjectInfoByUuid(uuid);
    }

    @Override
    public List<SyObjectInfo> selectSyObjectsByDatasetId(String datasetId){
        return syObjectInfoMapper.selectSyObjectsByDatasetId(datasetId);
    }
}
