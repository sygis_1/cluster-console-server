package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyProject;
import com.ruoyi.system.mapper.SyProjectMapper;
import com.ruoyi.system.service.ISyProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 项目信息表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-13
 */
@Service
public class SyProjectServiceImpl implements ISyProjectService {

    @Autowired
    private SyProjectMapper syProjectMapper;

    /**
     * 查询项目信息表
     * 
     * @param uuid 项目信息表主键
     * @return 项目信息表
     */
    @Override
    public SyProject selectSyProjectByUuid(String uuid)
    {
        return syProjectMapper.selectSyProjectByUuid(uuid);
    }

    /**
     * 查询项目信息表列表
     * 
     * @param syProject 项目信息表
     * @return 项目信息表
     */
    @Override
    public List<SyProject> selectSyProjectList(SyProject syProject)
    {
        return syProjectMapper.selectSyProjectList(syProject);
    }

    /**
     * 新增项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    @Override
    public int insertSyProject(SyProject syProject) {
        syProject.setCreateTime(DateUtils.getNowDate());
        return syProjectMapper.insertSyProject(syProject);
    }

    /**
     * 修改项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    @Override
    public int updateSyProject(SyProject syProject)
    {
        syProject.setUpdateTime(DateUtils.getNowDate());
        return syProjectMapper.updateSyProject(syProject);
    }

    /**
     * 批量删除项目信息表
     * 
     * @param uuids 需要删除的项目信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyProjectByUuids(String[] uuids)
    {
        return syProjectMapper.deleteSyProjectByUuids(uuids);
    }

    /**
     * 删除项目信息表信息
     * 
     * @param uuid 项目信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyProjectByUuid(String uuid) {
        return syProjectMapper.deleteSyProjectByUuid(uuid);
    }

    @Override
    public List<SyProject> selectProjectList4Select(){
        return syProjectMapper.selectProjectList4Select();
    }

    @Override
    public List<SyProject> selectUnbindRoleList(String roleIds){
        return syProjectMapper.selectUnbindRoleList(roleIds);
    }

    @Override
    public List<SyProject> selectBindRoleList(String roleIds){
        return syProjectMapper.selectBindRoleList(roleIds);
    }

    @Override
    public List<SyProject> selectDatasetProjectList(){
        return syProjectMapper.selectDatasetProjectList();
    }
}
