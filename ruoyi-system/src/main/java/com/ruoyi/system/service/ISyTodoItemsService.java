package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SyTodoItems;

/**
 * 待办事项Service接口
 * 
 * @author Sy
 * @date 2023-10-08
 */
public interface ISyTodoItemsService 
{
    /**
     * 查询待办事项
     * 
     * @param uuid 待办事项主键
     * @return 待办事项
     */
    public SyTodoItems selectSyTodoItemsByUuid(String uuid);

    /**
     * 查询待办事项列表
     * 
     * @param syTodoItems 待办事项
     * @return 待办事项集合
     */
    public List<SyTodoItems> selectSyTodoItemsList(SyTodoItems syTodoItems);

    public List<SyTodoItems> selectSyTodoItemsList1(SyTodoItems syTodoItems);

    /**
     * 新增待办事项
     * 
     * @param syTodoItems 待办事项
     * @return 结果
     */
    public int insertSyTodoItems(SyTodoItems syTodoItems);

    /**
     * 修改待办事项
     * 
     * @param syTodoItems 待办事项
     * @return 结果
     */
    public int updateSyTodoItems(SyTodoItems syTodoItems);

    /**
     * 批量删除待办事项
     * 
     * @param uuids 需要删除的待办事项主键集合
     * @return 结果
     */
    public int deleteSyTodoItemsByUuids(String[] uuids);

    /**
     * 删除待办事项信息
     * 
     * @param uuid 待办事项主键
     * @return 结果
     */
    public int deleteSyTodoItemsByUuid(String uuid);
}
