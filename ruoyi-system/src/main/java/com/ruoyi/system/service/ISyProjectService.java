package com.ruoyi.system.service;

import com.ruoyi.system.domain.SyProject;

import java.util.List;

/**
 * 项目信息表Service接口
 * 
 * @author Sy
 * @date 2023-09-13
 */
public interface ISyProjectService {

    /**
     * 查询项目信息表
     * 
     * @param uuid 项目信息表主键
     * @return 项目信息表
     */
    public SyProject selectSyProjectByUuid(String uuid);

    /**
     * 查询项目信息表列表
     * 
     * @param syProject 项目信息表
     * @return 项目信息表集合
     */
    public List<SyProject> selectSyProjectList(SyProject syProject);

    /**
     * 新增项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    public int insertSyProject(SyProject syProject);

    /**
     * 修改项目信息表
     * 
     * @param syProject 项目信息表
     * @return 结果
     */
    public int updateSyProject(SyProject syProject);

    /**
     * 批量删除项目信息表
     * 
     * @param uuids 需要删除的项目信息表主键集合
     * @return 结果
     */
    public int deleteSyProjectByUuids(String[] uuids);

    /**
     * 删除项目信息表信息
     * 
     * @param uuid 项目信息表主键
     * @return 结果
     */
    public int deleteSyProjectByUuid(String uuid);

    public List<SyProject> selectProjectList4Select();

    public List<SyProject> selectUnbindRoleList(String roleIds);

    public List<SyProject> selectBindRoleList(String roleIds);

    public List<SyProject> selectDatasetProjectList();
}
