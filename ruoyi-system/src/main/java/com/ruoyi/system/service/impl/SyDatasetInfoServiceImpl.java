package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyDatasetInfoMapper;
import com.ruoyi.system.domain.SyDatasetInfo;
import com.ruoyi.system.service.ISyDatasetInfoService;

/**
 * 数据集Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-26
 */
@Service
public class SyDatasetInfoServiceImpl implements ISyDatasetInfoService 
{
    @Autowired
    private SyDatasetInfoMapper syDatasetInfoMapper;

    /**
     * 查询数据集
     * 
     * @param uuid 数据集主键
     * @return 数据集
     */
    @Override
    public SyDatasetInfo selectSyDatasetInfoByUuid(String uuid) {
        return syDatasetInfoMapper.selectSyDatasetInfoByUuid(uuid);
    }

    /**
     * 查询数据集列表
     * 
     * @param syDatasetInfo 数据集
     * @return 数据集
     */
    @Override
    public List<SyDatasetInfo> selectSyDatasetInfoList(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.selectSyDatasetInfoList(syDatasetInfo);
    }

    @Override
    public List<SyDatasetInfo> selectUploadDataList(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.selectUploadDataList(syDatasetInfo);
    }

    @Override
    public List<SyDatasetInfo> selectPublishDataList(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.selectPublishDataList(syDatasetInfo);
    }

    @Override
    public List<SyDatasetInfo> listQueryDataset(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.listQueryDataset(syDatasetInfo);
    }

    @Override
    public List<SyDatasetInfo> selectOffShelfDataList(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.selectOffShelfDataList(syDatasetInfo);
    }

    /**
     * 新增数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    @Override
    public int insertSyDatasetInfo(SyDatasetInfo syDatasetInfo) {
        syDatasetInfo.setCreateTime(DateUtils.getNowDate());
        return syDatasetInfoMapper.insertSyDatasetInfo(syDatasetInfo);
    }

    /**
     * 修改数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    @Override
    public int updateSyDatasetInfo(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.updateSyDatasetInfo(syDatasetInfo);
    }

    @Override
    public int publishSyDatasetInfo(SyDatasetInfo syDatasetInfo) {
        return syDatasetInfoMapper.publishSyDatasetInfo(syDatasetInfo);
    }

    /**
     * 批量删除数据集
     * 
     * @param uuids 需要删除的数据集主键
     * @return 结果
     */
    @Override
    public int deleteSyDatasetInfoByUuids(String[] uuids) {
        return syDatasetInfoMapper.deleteSyDatasetInfoByUuids(uuids);
    }

    /**
     * 删除数据集信息
     * 
     * @param uuid 数据集主键
     * @return 结果
     */
    @Override
    public int deleteSyDatasetInfoByUuid(String uuid) {
        return syDatasetInfoMapper.deleteSyDatasetInfoByUuid(uuid);
    }

    @Override
    public List<SyDatasetInfo> selectUnbindStation(String stationIds){
        return syDatasetInfoMapper.selectUnbindStation(stationIds);
    }

    @Override
    public List<SyDatasetInfo> selectBindStation(String stationIds){
        return syDatasetInfoMapper.selectBindStation(stationIds);
    }

    @Override
    public List<SyDatasetInfo> selectUnbindProject(String projectIds){
        return syDatasetInfoMapper.selectUnbindProject(projectIds);
    }

    @Override
    public List<SyDatasetInfo> selectBindProject(String projectIds){
        return syDatasetInfoMapper.selectBindProject(projectIds);
    }

    @Override
    public List<SyDatasetInfo> selectUnbindEngineer(String engineerIds){
        return syDatasetInfoMapper.selectUnbindEngineer(engineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectBindEngineer(String engineerIds){
        return syDatasetInfoMapper.selectBindEngineer(engineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectUnbindSubEngineer(String subEngineerIds){
        return syDatasetInfoMapper.selectUnbindSubEngineer(subEngineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectBindSubEngineer(String subEngineerIds){
        return syDatasetInfoMapper.selectBindSubEngineer(subEngineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByUuidAndProjectId(String uuid,String projectIds){
        return syDatasetInfoMapper.selectDatasetByUuidAndProjectId(uuid,projectIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByUuidAndEngineerId(String uuid,String engineerIds){
        return syDatasetInfoMapper.selectDatasetByUuidAndEngineerId(uuid,engineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByUuidAndSubEngineerId(String uuid,String subEngineerIds){
        return syDatasetInfoMapper.selectDatasetByUuidAndSubEngineerId(uuid,subEngineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByUuidAndStationId(String uuid,String stationIds){
        return syDatasetInfoMapper.selectDatasetByUuidAndStationId(uuid,stationIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByProjectId(String projectIds){
        return syDatasetInfoMapper.selectDatasetByProjectId(projectIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByEngineerId(String engineerIds){
        return syDatasetInfoMapper.selectDatasetByEngineerId(engineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetBySubEngineerId(String subEngineerIds){
        return syDatasetInfoMapper.selectDatasetBySubEngineerId(subEngineerIds);
    }

    @Override
    public List<SyDatasetInfo> selectDatasetByStationId(String stationIds){
        return syDatasetInfoMapper.selectDatasetByStationId(stationIds);
    }
}
