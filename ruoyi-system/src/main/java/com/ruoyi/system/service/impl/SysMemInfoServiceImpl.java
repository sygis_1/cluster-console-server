package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysMemInfoMapper;
import com.ruoyi.system.domain.SysMemInfo;
import com.ruoyi.system.service.ISysMemInfoService;

/**
 * 内存信息表Service业务层处理
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@Service
public class SysMemInfoServiceImpl implements ISysMemInfoService 
{
    @Autowired
    private SysMemInfoMapper sysMemInfoMapper;

    /**
     * 查询内存信息表
     * 
     * @param serverIp 内存信息表主键
     * @return 内存信息表
     */
    @Override
    public SysMemInfo selectSysMemInfoByServerIp(String serverIp)
    {
        return sysMemInfoMapper.selectSysMemInfoByServerIp(serverIp);
    }

    /**
     * 查询内存信息表列表
     * 
     * @param sysMemInfo 内存信息表
     * @return 内存信息表
     */
    @Override
    public List<SysMemInfo> selectSysMemInfoList(SysMemInfo sysMemInfo)
    {
        return sysMemInfoMapper.selectSysMemInfoList(sysMemInfo);
    }

    /**
     * 新增内存信息表
     * 
     * @param sysMemInfo 内存信息表
     * @return 结果
     */
    @Override
    public int insertSysMemInfo(SysMemInfo sysMemInfo)
    {
        return sysMemInfoMapper.insertSysMemInfo(sysMemInfo);
    }

    /**
     * 修改内存信息表
     * 
     * @param sysMemInfo 内存信息表
     * @return 结果
     */
    @Override
    public int updateSysMemInfo(SysMemInfo sysMemInfo)
    {
        return sysMemInfoMapper.updateSysMemInfo(sysMemInfo);
    }

    /**
     * 批量删除内存信息表
     * 
     * @param serverIps 需要删除的内存信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysMemInfoByServerIps(String[] serverIps)
    {
        return sysMemInfoMapper.deleteSysMemInfoByServerIps(serverIps);
    }

    /**
     * 删除内存信息表信息
     * 
     * @param serverIp 内存信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysMemInfoByServerIp(String serverIp)
    {
        return sysMemInfoMapper.deleteSysMemInfoByServerIp(serverIp);
    }
}
