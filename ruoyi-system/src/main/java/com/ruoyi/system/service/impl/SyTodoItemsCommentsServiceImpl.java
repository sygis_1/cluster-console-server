package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyTodoItemsCommentsMapper;
import com.ruoyi.system.domain.SyTodoItemsComments;
import com.ruoyi.system.service.ISyTodoItemsCommentsService;

/**
 * 待办事项追述Service业务层处理
 * 
 * @author Sy
 * @date 2023-10-08
 */
@Service
public class SyTodoItemsCommentsServiceImpl implements ISyTodoItemsCommentsService 
{
    @Autowired
    private SyTodoItemsCommentsMapper syTodoItemsCommentsMapper;

    /**
     * 查询待办事项追述
     * 
     * @param uuid 待办事项追述主键
     * @return 待办事项追述
     */
    @Override
    public SyTodoItemsComments selectSyTodoItemsCommentsByUuid(String uuid)
    {
        return syTodoItemsCommentsMapper.selectSyTodoItemsCommentsByUuid(uuid);
    }

    /**
     * 查询待办事项追述列表
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 待办事项追述
     */
    @Override
    public List<SyTodoItemsComments> selectSyTodoItemsCommentsList(SyTodoItemsComments syTodoItemsComments)
    {
        return syTodoItemsCommentsMapper.selectSyTodoItemsCommentsList(syTodoItemsComments);
    }

    /**
     * 新增待办事项追述
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 结果
     */
    @Override
    public int insertSyTodoItemsComments(SyTodoItemsComments syTodoItemsComments)
    {
        syTodoItemsComments.setCreateTime(DateUtils.getNowDate());
        return syTodoItemsCommentsMapper.insertSyTodoItemsComments(syTodoItemsComments);
    }

    /**
     * 修改待办事项追述
     * 
     * @param syTodoItemsComments 待办事项追述
     * @return 结果
     */
    @Override
    public int updateSyTodoItemsComments(SyTodoItemsComments syTodoItemsComments)
    {
        return syTodoItemsCommentsMapper.updateSyTodoItemsComments(syTodoItemsComments);
    }

    /**
     * 批量删除待办事项追述
     * 
     * @param uuids 需要删除的待办事项追述主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsCommentsByUuids(String[] uuids)
    {
        return syTodoItemsCommentsMapper.deleteSyTodoItemsCommentsByUuids(uuids);
    }

    /**
     * 删除待办事项追述信息
     * 
     * @param uuid 待办事项追述主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsCommentsByUuid(String uuid)
    {
        return syTodoItemsCommentsMapper.deleteSyTodoItemsCommentsByUuid(uuid);
    }
}
