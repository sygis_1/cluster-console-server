package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SyFileInfo;

/**
 * 附件表Service接口
 * 
 * @author Sy
 * @date 2023-09-15
 */
public interface ISyFileInfoService 
{
    /**
     * 查询附件表
     * 
     * @param uuid 附件表主键
     * @return 附件表
     */
    public SyFileInfo selectSyFileInfoByUuid(String uuid);

    /**
     * 查询附件表列表
     * 
     * @param syFileInfo 附件表
     * @return 附件表集合
     */
    public List<SyFileInfo> selectSyFileInfoList(SyFileInfo syFileInfo);

    /**
     * 新增附件表
     * 
     * @param syFileInfo 附件表
     * @return 结果
     */
    public int insertSyFileInfo(SyFileInfo syFileInfo);

    /**
     * 修改附件表
     * 
     * @param syFileInfo 附件表
     * @return 结果
     */
    public int updateSyFileInfo(SyFileInfo syFileInfo);

    /**
     * 批量删除附件表
     * 
     * @param uuids 需要删除的附件表主键集合
     * @return 结果
     */
    public int deleteSyFileInfoByUuids(String[] uuids);

    /**
     * 删除附件表信息
     * 
     * @param uuid 附件表主键
     * @return 结果
     */
    public int deleteSyFileInfoByUuid(String uuid);
}
