package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyTodoItemsMapper;
import com.ruoyi.system.domain.SyTodoItems;
import com.ruoyi.system.service.ISyTodoItemsService;

/**
 * 待办事项Service业务层处理
 * 
 * @author Sy
 * @date 2023-10-08
 */
@Service
public class SyTodoItemsServiceImpl implements ISyTodoItemsService 
{
    @Autowired
    private SyTodoItemsMapper syTodoItemsMapper;

    /**
     * 查询待办事项
     * 
     * @param uuid 待办事项主键
     * @return 待办事项
     */
    @Override
    public SyTodoItems selectSyTodoItemsByUuid(String uuid)
    {
        return syTodoItemsMapper.selectSyTodoItemsByUuid(uuid);
    }

    /**
     * 查询待办事项列表
     * 
     * @param syTodoItems 待办事项
     * @return 待办事项
     */
    @Override
    public List<SyTodoItems> selectSyTodoItemsList(SyTodoItems syTodoItems)
    {
        return syTodoItemsMapper.selectSyTodoItemsList(syTodoItems);
    }

    @Override
    public List<SyTodoItems> selectSyTodoItemsList1(SyTodoItems syTodoItems)
    {
        return syTodoItemsMapper.selectSyTodoItemsList1(syTodoItems);
    }

    /**
     * 新增待办事项
     * 
     * @param syTodoItems 待办事项
     * @return 结果
     */
    @Override
    public int insertSyTodoItems(SyTodoItems syTodoItems)
    {
        syTodoItems.setCreateTime(DateUtils.getNowDate());
        return syTodoItemsMapper.insertSyTodoItems(syTodoItems);
    }

    /**
     * 修改待办事项
     * 
     * @param syTodoItems 待办事项
     * @return 结果
     */
    @Override
    public int updateSyTodoItems(SyTodoItems syTodoItems)
    {
        return syTodoItemsMapper.updateSyTodoItems(syTodoItems);
    }

    /**
     * 批量删除待办事项
     * 
     * @param uuids 需要删除的待办事项主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsByUuids(String[] uuids)
    {
        return syTodoItemsMapper.deleteSyTodoItemsByUuids(uuids);
    }

    /**
     * 删除待办事项信息
     * 
     * @param uuid 待办事项主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsByUuid(String uuid)
    {
        return syTodoItemsMapper.deleteSyTodoItemsByUuid(uuid);
    }
}
