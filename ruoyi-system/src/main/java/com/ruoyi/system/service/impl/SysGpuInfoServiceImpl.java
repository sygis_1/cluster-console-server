package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysGpuInfoMapper;
import com.ruoyi.system.domain.SysGpuInfo;
import com.ruoyi.system.service.ISysGpuInfoService;

/**
 * GPU信息表Service业务层处理
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@Service
public class SysGpuInfoServiceImpl implements ISysGpuInfoService 
{
    @Autowired
    private SysGpuInfoMapper sysGpuInfoMapper;

    /**
     * 查询GPU信息表
     * 
     * @param serverIp GPU信息表主键
     * @return GPU信息表
     */
    @Override
    public SysGpuInfo selectSysGpuInfoByServerIp(String serverIp)
    {
        return sysGpuInfoMapper.selectSysGpuInfoByServerIp(serverIp);
    }

    /**
     * 查询GPU信息表列表
     * 
     * @param sysGpuInfo GPU信息表
     * @return GPU信息表
     */
    @Override
    public List<SysGpuInfo> selectSysGpuInfoList(SysGpuInfo sysGpuInfo)
    {
        return sysGpuInfoMapper.selectSysGpuInfoList(sysGpuInfo);
    }

    /**
     * 新增GPU信息表
     * 
     * @param sysGpuInfo GPU信息表
     * @return 结果
     */
    @Override
    public int insertSysGpuInfo(SysGpuInfo sysGpuInfo)
    {
        return sysGpuInfoMapper.insertSysGpuInfo(sysGpuInfo);
    }

    /**
     * 修改GPU信息表
     * 
     * @param sysGpuInfo GPU信息表
     * @return 结果
     */
    @Override
    public int updateSysGpuInfo(SysGpuInfo sysGpuInfo)
    {
        return sysGpuInfoMapper.updateSysGpuInfo(sysGpuInfo);
    }

    /**
     * 批量删除GPU信息表
     * 
     * @param serverIps 需要删除的GPU信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysGpuInfoByServerIps(String[] serverIps)
    {
        return sysGpuInfoMapper.deleteSysGpuInfoByServerIps(serverIps);
    }

    /**
     * 删除GPU信息表信息
     * 
     * @param serverIp GPU信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysGpuInfoByServerIp(String serverIp)
    {
        return sysGpuInfoMapper.deleteSysGpuInfoByServerIp(serverIp);
    }
}
