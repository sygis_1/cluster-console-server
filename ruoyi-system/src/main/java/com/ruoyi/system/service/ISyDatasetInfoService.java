package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SyDatasetInfo;

/**
 * 数据集Service接口
 * 
 * @author Sy
 * @date 2023-09-26
 */
public interface ISyDatasetInfoService 
{
    /**
     * 查询数据集
     * 
     * @param uuid 数据集主键
     * @return 数据集
     */
    public SyDatasetInfo selectSyDatasetInfoByUuid(String uuid);

    /**
     * 查询数据集列表
     * 
     * @param syDatasetInfo 数据集
     * @return 数据集集合
     */
    public List<SyDatasetInfo> selectSyDatasetInfoList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectUploadDataList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectPublishDataList(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> listQueryDataset(SyDatasetInfo syDatasetInfo);

    public List<SyDatasetInfo> selectOffShelfDataList(SyDatasetInfo syDatasetInfo);

    /**
     * 新增数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    public int insertSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    /**
     * 修改数据集
     * 
     * @param syDatasetInfo 数据集
     * @return 结果
     */
    public int updateSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    public int publishSyDatasetInfo(SyDatasetInfo syDatasetInfo);

    /**
     * 批量删除数据集
     * 
     * @param uuids 需要删除的数据集主键集合
     * @return 结果
     */
    public int deleteSyDatasetInfoByUuids(String[] uuids);

    /**
     * 删除数据集信息
     * 
     * @param uuid 数据集主键
     * @return 结果
     */
    public int deleteSyDatasetInfoByUuid(String uuid);

    public List<SyDatasetInfo> selectUnbindStation(String stationIds);

    public List<SyDatasetInfo> selectBindStation(String stationIds);

    public List<SyDatasetInfo> selectUnbindProject(String projectIds);

    public List<SyDatasetInfo> selectBindProject(String projectIds);

    public List<SyDatasetInfo> selectUnbindEngineer(String engineerIds);

    public List<SyDatasetInfo> selectBindEngineer(String engineerIds);

    public List<SyDatasetInfo> selectUnbindSubEngineer(String subEngineerIds);

    public List<SyDatasetInfo> selectBindSubEngineer(String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndProjectId(String uuid,String projectIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndEngineerId(String uuid,String engineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndSubEngineerId(String uuid,String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByUuidAndStationId(String uuid,String stationIds);

    public List<SyDatasetInfo> selectDatasetByProjectId(String projectIds);

    public List<SyDatasetInfo> selectDatasetByEngineerId(String engineerIds);

    public List<SyDatasetInfo> selectDatasetBySubEngineerId(String subEngineerIds);

    public List<SyDatasetInfo> selectDatasetByStationId(String stationIds);
}
