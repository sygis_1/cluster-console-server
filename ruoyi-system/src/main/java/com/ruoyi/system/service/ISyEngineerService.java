package com.ruoyi.system.service;

import com.ruoyi.system.domain.SyEngineer;

import java.util.List;

/**
 * 工程信息表Service接口
 * 
 * @author Sy
 * @date 2023-09-13
 */
public interface ISyEngineerService {

    /**
     * 查询工程信息表
     * 
     * @param uuid 工程信息表主键
     * @return 工程信息表
     */
    public SyEngineer selectSyEngineerByUuid(String uuid);

    /**
     * 查询工程信息表列表
     * 
     * @param syEngineer 工程信息表
     * @return 工程信息表集合
     */
    public List<SyEngineer> selectSyEngineerList(SyEngineer syEngineer);

    public List<SyEngineer> selectSyEngineerList4Select();

    public List<SyEngineer> getEngineerList4Project(SyEngineer syEngineer);

    /**
     * 新增工程信息表
     * 
     * @param syEngineer 工程信息表
     * @return 结果
     */
    public int insertSyEngineer(SyEngineer syEngineer);

    /**
     * 修改工程信息表
     * 
     * @param syEngineer 工程信息表
     * @return 结果
     */
    public int updateSyEngineer(SyEngineer syEngineer);

    public int releaseEngineer(String uuid);

    /**
     * 批量删除工程信息表
     * 
     * @param uuids 需要删除的工程信息表主键集合
     * @return 结果
     */
    public int deleteSyEngineerByUuids(String[] uuids);

    /**
     * 删除工程信息表信息
     * 
     * @param uuid 工程信息表主键
     * @return 结果
     */
    public int deleteSyEngineerByUuid(String uuid);

    public List<SyEngineer> selectUnbindRoleList(String roleIds);

    public List<SyEngineer> selectBindRoleList(String roleIds);

    public List<SyEngineer> selectDatasetEngineerList();
}
