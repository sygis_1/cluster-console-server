package com.ruoyi.system.service;

import com.ruoyi.system.domain.SySubEngineer;

import java.util.List;

/**
 * 子工程信息表Service接口
 * 
 * @author Sy
 * @date 2023-09-13
 */
public interface ISySubEngineerService {

    /**
     * 查询子工程信息表
     * 
     * @param uuid 子工程信息表主键
     * @return 子工程信息表
     */
    public SySubEngineer selectSySubEngineerByUuid(String uuid);

    /**
     * 查询子工程信息表列表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 子工程信息表集合
     */
    public List<SySubEngineer> selectSySubEngineerList(SySubEngineer sySubEngineer);

    public List<SySubEngineer> selectSubEngineerList4Engineer(SySubEngineer sySubEngineer);

    /**
     * 新增子工程信息表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 结果
     */
    public int insertSySubEngineer(SySubEngineer sySubEngineer);

    /**
     * 修改子工程信息表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 结果
     */
    public int updateSySubEngineer(SySubEngineer sySubEngineer);

    public int releaseSubEngineerByUuid(String uuid);

    /**
     * 批量删除子工程信息表
     * 
     * @param uuids 需要删除的子工程信息表主键集合
     * @return 结果
     */
    public int deleteSySubEngineerByUuids(String[] uuids);

    /**
     * 删除子工程信息表信息
     * 
     * @param uuid 子工程信息表主键
     * @return 结果
     */
    public int deleteSySubEngineerByUuid(String uuid);

    public List<SySubEngineer> selectUnbindRoleList(String roleIds);

    public List<SySubEngineer> selectBindRoleList(String roleIds);

    public List<SySubEngineer> selectDatasetSubEngineerList();

    public List<SySubEngineer> selectSubEngineerList4Select();
}
