package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysCpuInfoMapper;
import com.ruoyi.system.domain.SysCpuInfo;
import com.ruoyi.system.service.ISysCpuInfoService;

/**
 * cpu信息Service业务层处理
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@Service
public class SysCpuInfoServiceImpl implements ISysCpuInfoService 
{
    @Autowired
    private SysCpuInfoMapper sysCpuInfoMapper;

    /**
     * 查询cpu信息
     * 
     * @param serverIp cpu信息主键
     * @return cpu信息
     */
    @Override
    public SysCpuInfo selectSysCpuInfoByServerIp(String serverIp)
    {
        return sysCpuInfoMapper.selectSysCpuInfoByServerIp(serverIp);
    }

    /**
     * 查询cpu信息列表
     * 
     * @param sysCpuInfo cpu信息
     * @return cpu信息
     */
    @Override
    public List<SysCpuInfo> selectSysCpuInfoList(SysCpuInfo sysCpuInfo)
    {
        return sysCpuInfoMapper.selectSysCpuInfoList(sysCpuInfo);
    }

    /**
     * 新增cpu信息
     * 
     * @param sysCpuInfo cpu信息
     * @return 结果
     */
    @Override
    public int insertSysCpuInfo(SysCpuInfo sysCpuInfo)
    {
        return sysCpuInfoMapper.insertSysCpuInfo(sysCpuInfo);
    }

    /**
     * 修改cpu信息
     * 
     * @param sysCpuInfo cpu信息
     * @return 结果
     */
    @Override
    public int updateSysCpuInfo(SysCpuInfo sysCpuInfo)
    {
        return sysCpuInfoMapper.updateSysCpuInfo(sysCpuInfo);
    }

    /**
     * 批量删除cpu信息
     * 
     * @param serverIps 需要删除的cpu信息主键
     * @return 结果
     */
    @Override
    public int deleteSysCpuInfoByServerIps(String[] serverIps)
    {
        return sysCpuInfoMapper.deleteSysCpuInfoByServerIps(serverIps);
    }

    /**
     * 删除cpu信息信息
     * 
     * @param serverIp cpu信息主键
     * @return 结果
     */
    @Override
    public int deleteSysCpuInfoByServerIp(String serverIp)
    {
        return sysCpuInfoMapper.deleteSysCpuInfoByServerIp(serverIp);
    }
}
