package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SyObjectInfo;

/**
 * 数据对象Service接口
 * 
 * @author Sy
 * @date 2023-09-26
 */
public interface ISyObjectInfoService 
{
    /**
     * 查询数据对象
     * 
     * @param uuid 数据对象主键
     * @return 数据对象
     */
    public SyObjectInfo selectSyObjectInfoByUuid(String uuid);

    /**
     * 查询数据对象列表
     * 
     * @param syObjectInfo 数据对象
     * @return 数据对象集合
     */
    public List<SyObjectInfo> selectSyObjectInfoList(SyObjectInfo syObjectInfo);

    /**
     * 新增数据对象
     * 
     * @param syObjectInfo 数据对象
     * @return 结果
     */
    public int insertSyObjectInfo(SyObjectInfo syObjectInfo);

    /**
     * 修改数据对象
     * 
     * @param syObjectInfo 数据对象
     * @return 结果
     */
    public int updateSyObjectInfo(SyObjectInfo syObjectInfo);

    /**
     * 批量删除数据对象
     * 
     * @param uuids 需要删除的数据对象主键集合
     * @return 结果
     */
    public int deleteSyObjectInfoByUuids(String[] uuids);

    /**
     * 删除数据对象信息
     * 
     * @param uuid 数据对象主键
     * @return 结果
     */
    public int deleteSyObjectInfoByUuid(String uuid);

    public List<SyObjectInfo> selectSyObjectsByDatasetId(String datasetId);
}
