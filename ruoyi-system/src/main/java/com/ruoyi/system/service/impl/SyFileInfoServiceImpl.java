package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyFileInfoMapper;
import com.ruoyi.system.domain.SyFileInfo;
import com.ruoyi.system.service.ISyFileInfoService;

/**
 * 附件表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-15
 */
@Service
public class SyFileInfoServiceImpl implements ISyFileInfoService 
{
    @Autowired
    private SyFileInfoMapper syFileInfoMapper;

    /**
     * 查询附件表
     * 
     * @param uuid 附件表主键
     * @return 附件表
     */
    @Override
    public SyFileInfo selectSyFileInfoByUuid(String uuid)
    {
        return syFileInfoMapper.selectSyFileInfoByUuid(uuid);
    }

    /**
     * 查询附件表列表
     * 
     * @param syFileInfo 附件表
     * @return 附件表
     */
    @Override
    public List<SyFileInfo> selectSyFileInfoList(SyFileInfo syFileInfo)
    {
        return syFileInfoMapper.selectSyFileInfoList(syFileInfo);
    }

    /**
     * 新增附件表
     * 
     * @param syFileInfo 附件表
     * @return 结果
     */
    @Override
    public int insertSyFileInfo(SyFileInfo syFileInfo)
    {
        return syFileInfoMapper.insertSyFileInfo(syFileInfo);
    }

    /**
     * 修改附件表
     * 
     * @param syFileInfo 附件表
     * @return 结果
     */
    @Override
    public int updateSyFileInfo(SyFileInfo syFileInfo)
    {
        return syFileInfoMapper.updateSyFileInfo(syFileInfo);
    }

    /**
     * 批量删除附件表
     * 
     * @param uuids 需要删除的附件表主键
     * @return 结果
     */
    @Override
    public int deleteSyFileInfoByUuids(String[] uuids)
    {
        return syFileInfoMapper.deleteSyFileInfoByUuids(uuids);
    }

    /**
     * 删除附件表信息
     * 
     * @param uuid 附件表主键
     * @return 结果
     */
    @Override
    public int deleteSyFileInfoByUuid(String uuid)
    {
        return syFileInfoMapper.deleteSyFileInfoByUuid(uuid);
    }
}
