package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SySubEngineer;
import com.ruoyi.system.mapper.SySubEngineerMapper;
import com.ruoyi.system.service.ISySubEngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 子工程信息表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-13
 */
@Service
public class SySubEngineerServiceImpl implements ISySubEngineerService {

    @Autowired
    private SySubEngineerMapper sySubEngineerMapper;

    /**
     * 查询子工程信息表
     * 
     * @param uuid 子工程信息表主键
     * @return 子工程信息表
     */
    @Override
    public SySubEngineer selectSySubEngineerByUuid(String uuid)
    {
        return sySubEngineerMapper.selectSySubEngineerByUuid(uuid);
    }

    /**
     * 查询子工程信息表列表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 子工程信息表
     */
    @Override
    public List<SySubEngineer> selectSySubEngineerList(SySubEngineer sySubEngineer) {
        return sySubEngineerMapper.selectSySubEngineerList(sySubEngineer);
    }

    @Override
    public List<SySubEngineer> selectSubEngineerList4Engineer(SySubEngineer sySubEngineer) {
        return sySubEngineerMapper.selectSubEngineerList4Engineer(sySubEngineer);
    }

    /**
     * 新增子工程信息表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 结果
     */
    @Override
    public int insertSySubEngineer(SySubEngineer sySubEngineer)
    {
        sySubEngineer.setCreateTime(DateUtils.getNowDate());
        return sySubEngineerMapper.insertSySubEngineer(sySubEngineer);
    }

    /**
     * 修改子工程信息表
     * 
     * @param sySubEngineer 子工程信息表
     * @return 结果
     */
    @Override
    public int updateSySubEngineer(SySubEngineer sySubEngineer) {
        sySubEngineer.setUpdateTime(DateUtils.getNowDate());
        return sySubEngineerMapper.updateSySubEngineer(sySubEngineer);
    }

    @Override
    public int releaseSubEngineerByUuid(String uuid) {
        return sySubEngineerMapper.releaseSubEngineerByUuid(uuid);
    }

    /**
     * 批量删除子工程信息表
     * 
     * @param uuids 需要删除的子工程信息表主键
     * @return 结果
     */
    @Override
    public int deleteSySubEngineerByUuids(String[] uuids) {
        return sySubEngineerMapper.deleteSySubEngineerByUuids(uuids);
    }

    /**
     * 删除子工程信息表信息
     * 
     * @param uuid 子工程信息表主键
     * @return 结果
     */
    @Override
    public int deleteSySubEngineerByUuid(String uuid) {
        return sySubEngineerMapper.deleteSySubEngineerByUuid(uuid);
    }

    @Override
    public List<SySubEngineer> selectUnbindRoleList(String roleIds){
        return sySubEngineerMapper.selectUnbindRoleList(roleIds);
    }

    @Override
    public List<SySubEngineer> selectBindRoleList(String roleIds){
        return sySubEngineerMapper.selectBindRoleList(roleIds);
    }

    @Override
    public List<SySubEngineer> selectDatasetSubEngineerList(){
        return sySubEngineerMapper.selectDatasetSubEngineerList();
    }

    @Override
    public List<SySubEngineer> selectSubEngineerList4Select(){
        return sySubEngineerMapper.selectSubEngineerList4Select();
    }
}
