package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysEngineInfoMapper;
import com.ruoyi.system.domain.SysEngineInfo;
import com.ruoyi.system.service.ISysEngineInfoService;

/**
 * 引擎信息表Service业务层处理
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
@Service
public class SysEngineInfoServiceImpl implements ISysEngineInfoService 
{
    @Autowired
    private SysEngineInfoMapper sysEngineInfoMapper;

    /**
     * 查询引擎信息表
     * 
     * @param serverIp 引擎信息表主键
     * @return 引擎信息表
     */
    @Override
    public SysEngineInfo selectSysEngineInfoByServerIp(String serverIp)
    {
        return sysEngineInfoMapper.selectSysEngineInfoByServerIp(serverIp);
    }

    /**
     * 查询引擎信息表列表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 引擎信息表
     */
    @Override
    public List<SysEngineInfo> selectSysEngineInfoList(SysEngineInfo sysEngineInfo)
    {
        return sysEngineInfoMapper.selectSysEngineInfoList(sysEngineInfo);
    }

    /**
     * 新增引擎信息表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 结果
     */
    @Override
    public int insertSysEngineInfo(SysEngineInfo sysEngineInfo)
    {
        return sysEngineInfoMapper.insertSysEngineInfo(sysEngineInfo);
    }

    /**
     * 修改引擎信息表
     * 
     * @param sysEngineInfo 引擎信息表
     * @return 结果
     */
    @Override
    public int updateSysEngineInfo(SysEngineInfo sysEngineInfo)
    {
        return sysEngineInfoMapper.updateSysEngineInfo(sysEngineInfo);
    }

    /**
     * 批量删除引擎信息表
     * 
     * @param serverIps 需要删除的引擎信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysEngineInfoByServerIps(String[] serverIps)
    {
        return sysEngineInfoMapper.deleteSysEngineInfoByServerIps(serverIps);
    }

    /**
     * 删除引擎信息表信息
     * 
     * @param serverIp 引擎信息表主键
     * @return 结果
     */
    @Override
    public int deleteSysEngineInfoByServerIp(String serverIp)
    {
        return sysEngineInfoMapper.deleteSysEngineInfoByServerIp(serverIp);
    }
}
