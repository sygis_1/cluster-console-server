package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SyEngineer;
import com.ruoyi.system.mapper.SyEngineerMapper;
import com.ruoyi.system.service.ISyEngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工程信息表Service业务层处理
 * 
 * @author Sy
 * @date 2023-09-13
 */
@Service
public class SyEngineerServiceImpl implements ISyEngineerService {

    @Autowired
    private SyEngineerMapper syEngineerMapper;

    /**
     * 查询工程信息表
     * 
     * @param uuid 工程信息表主键
     * @return 工程信息表
     */
    @Override
    public SyEngineer selectSyEngineerByUuid(String uuid)
    {
        return syEngineerMapper.selectSyEngineerByUuid(uuid);
    }

    /**
     * 查询工程信息表列表
     * 
     * @param syEngineer 工程信息表
     * @return 工程信息表
     */
    @Override
    public List<SyEngineer> selectSyEngineerList(SyEngineer syEngineer) {
        return syEngineerMapper.selectSyEngineerList(syEngineer);
    }

    @Override
    public List<SyEngineer> selectSyEngineerList4Select() {
        return syEngineerMapper.selectSyEngineerList4Select();
    }

    @Override
    public List<SyEngineer> getEngineerList4Project(SyEngineer syEngineer) {
        return syEngineerMapper.getEngineerList4Project(syEngineer);
    }

    /**
     * 新增工程信息表
     * 
     * @param syEngineer 工程信息表
     * @return 结果
     */
    @Override
    public int insertSyEngineer(SyEngineer syEngineer) {
        syEngineer.setCreateTime(DateUtils.getNowDate());
        return syEngineerMapper.insertSyEngineer(syEngineer);
    }

    /**
     * 修改工程信息表
     * 
     * @param syEngineer 工程信息表
     * @return 结果
     */
    @Override
    public int updateSyEngineer(SyEngineer syEngineer) {
        syEngineer.setUpdateTime(DateUtils.getNowDate());
        return syEngineerMapper.updateSyEngineer(syEngineer);
    }

    @Override
    public int releaseEngineer(String uuid){
        return syEngineerMapper.releaseEngineer(uuid);
    }


    /**
     * 批量删除工程信息表
     * 
     * @param uuids 需要删除的工程信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyEngineerByUuids(String[] uuids)
    {
        return syEngineerMapper.deleteSyEngineerByUuids(uuids);
    }

    /**
     * 删除工程信息表信息
     * 
     * @param uuid 工程信息表主键
     * @return 结果
     */
    @Override
    public int deleteSyEngineerByUuid(String uuid) {
        return syEngineerMapper.deleteSyEngineerByUuid(uuid);
    }

    @Override
    public List<SyEngineer> selectUnbindRoleList(String roleIds){
        return syEngineerMapper.selectUnbindRoleList(roleIds);
    }

    @Override
    public List<SyEngineer> selectBindRoleList(String roleIds){
        return syEngineerMapper.selectBindRoleList(roleIds);
    }

    @Override
    public List<SyEngineer> selectDatasetEngineerList(){
        return syEngineerMapper.selectDatasetEngineerList();
    }
}
