package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SyTodoItemsFollowupMapper;
import com.ruoyi.system.domain.SyTodoItemsFollowup;
import com.ruoyi.system.service.ISyTodoItemsFollowupService;

/**
 * 待办事项跟进记录Service业务层处理
 * 
 * @author Sy
 * @date 2023-10-08
 */
@Service
public class SyTodoItemsFollowupServiceImpl implements ISyTodoItemsFollowupService 
{
    @Autowired
    private SyTodoItemsFollowupMapper syTodoItemsFollowupMapper;

    /**
     * 查询待办事项跟进记录
     * 
     * @param uuid 待办事项跟进记录主键
     * @return 待办事项跟进记录
     */
    @Override
    public SyTodoItemsFollowup selectSyTodoItemsFollowupByUuid(String uuid)
    {
        return syTodoItemsFollowupMapper.selectSyTodoItemsFollowupByUuid(uuid);
    }

    /**
     * 查询待办事项跟进记录列表
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 待办事项跟进记录
     */
    @Override
    public List<SyTodoItemsFollowup> selectSyTodoItemsFollowupList(SyTodoItemsFollowup syTodoItemsFollowup)
    {
        return syTodoItemsFollowupMapper.selectSyTodoItemsFollowupList(syTodoItemsFollowup);
    }

    /**
     * 新增待办事项跟进记录
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 结果
     */
    @Override
    public int insertSyTodoItemsFollowup(SyTodoItemsFollowup syTodoItemsFollowup)
    {
        syTodoItemsFollowup.setCreateTime(DateUtils.getNowDate());
        return syTodoItemsFollowupMapper.insertSyTodoItemsFollowup(syTodoItemsFollowup);
    }

    /**
     * 修改待办事项跟进记录
     * 
     * @param syTodoItemsFollowup 待办事项跟进记录
     * @return 结果
     */
    @Override
    public int updateSyTodoItemsFollowup(SyTodoItemsFollowup syTodoItemsFollowup)
    {
        return syTodoItemsFollowupMapper.updateSyTodoItemsFollowup(syTodoItemsFollowup);
    }

    /**
     * 批量删除待办事项跟进记录
     * 
     * @param uuids 需要删除的待办事项跟进记录主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsFollowupByUuids(String[] uuids)
    {
        return syTodoItemsFollowupMapper.deleteSyTodoItemsFollowupByUuids(uuids);
    }

    /**
     * 删除待办事项跟进记录信息
     * 
     * @param uuid 待办事项跟进记录主键
     * @return 结果
     */
    @Override
    public int deleteSyTodoItemsFollowupByUuid(String uuid)
    {
        return syTodoItemsFollowupMapper.deleteSyTodoItemsFollowupByUuid(uuid);
    }
}
