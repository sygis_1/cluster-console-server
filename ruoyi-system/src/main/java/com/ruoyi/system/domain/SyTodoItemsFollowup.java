package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 待办事项跟进记录对象 sy_todo_items_followup
 * 
 * @author Sy
 * @date 2023-10-08
 */
public class SyTodoItemsFollowup extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 事项标识 */
    @Excel(name = "事项标识")
    private String itemId;

    /** 跟进描述 */
    @Excel(name = "跟进描述")
    private String followupDesc;

    /** 附件 */
    @Excel(name = "附件")
    private String followupAttach;

    /** 跟进日期 */
    @Excel(name = "跟进日期")
    private String followupData;

    /** 跟进人 */
    @Excel(name = "跟进人")
    private Long followupId;

    /** 跟进人 */
    @Excel(name = "跟进人")
    private String followupPerson;

    private Long nextFollowupId;

    public Long getNextFollowupId() {
        return nextFollowupId;
    }

    public void setNextFollowupId(Long nextFollowupId) {
        this.nextFollowupId = nextFollowupId;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setItemId(String itemId) 
    {
        this.itemId = itemId;
    }

    public String getItemId() 
    {
        return itemId;
    }
    public void setFollowupDesc(String followupDesc) 
    {
        this.followupDesc = followupDesc;
    }

    public String getFollowupDesc() 
    {
        return followupDesc;
    }
    public void setFollowupAttach(String followupAttach) 
    {
        this.followupAttach = followupAttach;
    }

    public String getFollowupAttach() 
    {
        return followupAttach;
    }
    public void setFollowupData(String followupData) 
    {
        this.followupData = followupData;
    }

    public String getFollowupData() 
    {
        return followupData;
    }
    public void setFollowupId(Long followupId) 
    {
        this.followupId = followupId;
    }

    public Long getFollowupId() 
    {
        return followupId;
    }
    public void setFollowupPerson(String followupPerson) 
    {
        this.followupPerson = followupPerson;
    }

    public String getFollowupPerson() 
    {
        return followupPerson;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("itemId", getItemId())
            .append("followupDesc", getFollowupDesc())
            .append("followupAttach", getFollowupAttach())
            .append("followupData", getFollowupData())
            .append("followupId", getFollowupId())
            .append("followupPerson", getFollowupPerson())
            .append("createTime", getCreateTime())
            .toString();
    }
}
