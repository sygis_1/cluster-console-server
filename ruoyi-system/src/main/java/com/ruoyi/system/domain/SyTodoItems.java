package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 待办事项对象 sy_todo_items
 * 
 * @author Sy
 * @date 2023-10-08
 */
public class SyTodoItems extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 事项标题 */
    @Excel(name = "事项标题")
    private String itemName;

    /** 事项内容 */
    @Excel(name = "事项内容")
    private String itemDesc;

    /** 跟进人 */
    @Excel(name = "跟进人")
    private Long followupId;

    /** 跟进人姓名 */
    @Excel(name = "跟进人姓名")
    private String followupName;

    /** 发起人姓名 */
    @Excel(name = "发起人姓名")
    private String createName;

    /** 跟进状态	0-待跟进	1-跟进中	2-待办结束 */
    @Excel(name = "跟进状态	0-待跟进	1-跟进中	2-待办结束")
    private Integer status;

    /** 事项附件 */
    @Excel(name = "事项附件")
    private String attach;

    /** 截止日期 */
    @Excel(name = "截止日期")
    private String deadTime;

    /** 结束描述 */
    @Excel(name = "结束描述")
    private String annotation;

    /** 待办事项类型  1-项目事项 2-工程事项 3-子工程事项 4-站点事项 */
    @Excel(name = "待办事项类型  1-项目事项 2-工程事项 3-子工程事项 4-站点事项")
    private Integer itemType;

    /** 关联标识 */
    @Excel(name = "关联标识")
    private String itemParentId;

    private Integer editFlag;

    private String itemParentName;

    public String getItemParentName() {
        return itemParentName;
    }

    public void setItemParentName(String itemParentName) {
        this.itemParentName = itemParentName;
    }

    public Integer getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(Integer editFlag) {
        this.editFlag = editFlag;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setItemName(String itemName) 
    {
        this.itemName = itemName;
    }

    public String getItemName() 
    {
        return itemName;
    }
    public void setItemDesc(String itemDesc) 
    {
        this.itemDesc = itemDesc;
    }

    public String getItemDesc() 
    {
        return itemDesc;
    }
    public void setFollowupId(Long followupId) 
    {
        this.followupId = followupId;
    }

    public Long getFollowupId() 
    {
        return followupId;
    }
    public void setFollowupName(String followupName) 
    {
        this.followupName = followupName;
    }

    public String getFollowupName() 
    {
        return followupName;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setAttach(String attach) 
    {
        this.attach = attach;
    }

    public String getAttach() 
    {
        return attach;
    }
    public void setDeadTime(String deadTime) 
    {
        this.deadTime = deadTime;
    }

    public String getDeadTime() 
    {
        return deadTime;
    }
    public void setAnnotation(String annotation) 
    {
        this.annotation = annotation;
    }

    public String getAnnotation() 
    {
        return annotation;
    }
    public void setItemType(Integer itemType) 
    {
        this.itemType = itemType;
    }

    public Integer getItemType() 
    {
        return itemType;
    }
    public void setItemParentId(String itemParentId) 
    {
        this.itemParentId = itemParentId;
    }

    public String getItemParentId() 
    {
        return itemParentId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("itemName", getItemName())
            .append("itemDesc", getItemDesc())
            .append("followupId", getFollowupId())
            .append("followupName", getFollowupName())
            .append("createBy", getCreateBy())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .append("attach", getAttach())
            .append("remark", getRemark())
            .append("deadTime", getDeadTime())
            .append("annotation", getAnnotation())
            .append("itemType", getItemType())
            .append("itemParentId", getItemParentId())
            .toString();
    }
}
