package com.ruoyi.system.domain;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 内存信息表对象 sys_mem_info
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public class SysMemInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** IP */
    @Excel(name = "IP")
    private String serverIp;

    /** 机器名 */
    @Excel(name = "机器名")
    private String hostName;

    /** 操作系统 */
    @Excel(name = "操作系统")
    private String sysName;

    /** 系统架构 */
    @Excel(name = "系统架构")
    private String sysArch;

    /** 总内存 */
    @Excel(name = "总内存")
    private String memTotal;

    /** 使用内存 */
    @Excel(name = "使用内存")
    private String memUsed;

    /** 空闲内存 */
    @Excel(name = "空闲内存")
    private String memLeft;

    /** 内存使用率 */
    @Excel(name = "内存使用率")
    private String memUseRate;

    public void setServerIp(String serverIp) 
    {
        this.serverIp = serverIp;
    }

    public String getServerIp() 
    {
        return serverIp;
    }
    public void setHostName(String hostName) 
    {
        this.hostName = hostName;
    }

    public String getHostName() 
    {
        return hostName;
    }
    public void setSysName(String sysName) 
    {
        this.sysName = sysName;
    }

    public String getSysName() 
    {
        return sysName;
    }
    public void setSysArch(String sysArch) 
    {
        this.sysArch = sysArch;
    }

    public String getSysArch() 
    {
        return sysArch;
    }
    public void setMemTotal(String memTotal) 
    {
        this.memTotal = memTotal;
    }

    public String getMemTotal() 
    {
        return memTotal;
    }
    public void setMemUsed(String memUsed) 
    {
        this.memUsed = memUsed;
    }

    public String getMemUsed() 
    {
        return memUsed;
    }
    public void setMemLeft(String memLeft) 
    {
        this.memLeft = memLeft;
    }

    public String getMemLeft() 
    {
        return memLeft;
    }
    public void setMemUseRate(String memUseRate) 
    {
        this.memUseRate = memUseRate;
    }

    public String getMemUseRate() 
    {
        return memUseRate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("serverIp", getServerIp())
            .append("hostName", getHostName())
            .append("sysName", getSysName())
            .append("sysArch", getSysArch())
            .append("memTotal", getMemTotal())
            .append("memUsed", getMemUsed())
            .append("memLeft", getMemLeft())
            .append("memUseRate", getMemUseRate())
            .toString();
    }
}
