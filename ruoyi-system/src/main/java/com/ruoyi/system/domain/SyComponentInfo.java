package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 部件表对象 sy_component_info
 * 
 * @author Sy
 * @date 2023-09-28
 */
public class SyComponentInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 部件名称 */
    @Excel(name = "部件名称")
    private String name;

    /** 数据集标识 */
    @Excel(name = "数据集标识")
    private String datasetId;

    /** 部件描述 */
    @Excel(name = "部件描述")
    private String describe;

    /** 对象标识 */
    @Excel(name = "对象标识")
    private String objectId;

    /** 部件归类 */
    @Excel(name = "部件归类")
    private Integer type;

    /** 右幅图片附件 */
    @Excel(name = "右幅图片附件")
    private String rightImgAttach;

    /** 右幅其他附件 */
    @Excel(name = "右幅其他附件")
    private String rightFileAttach;

    /** 左幅图片附件 */
    @Excel(name = "左幅图片附件")
    private String leftImgAttach;

    /** 左幅其他附件 */
    @Excel(name = "左幅其他附件")
    private String leftFileAttach;

    /** 访问路径 */
    @Excel(name = "访问路径")
    private String path;

    private Long id;

    private String orginName;

    public String getOrginName() {
        return orginName;
    }

    public void setOrginName(String orginName) {
        this.orginName = orginName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDatasetId(String datasetId) 
    {
        this.datasetId = datasetId;
    }

    public String getDatasetId() 
    {
        return datasetId;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setObjectId(String objectId) 
    {
        this.objectId = objectId;
    }

    public String getObjectId() 
    {
        return objectId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setRightImgAttach(String rightImgAttach) 
    {
        this.rightImgAttach = rightImgAttach;
    }

    public String getRightImgAttach() 
    {
        return rightImgAttach;
    }
    public void setRightFileAttach(String rightFileAttach) 
    {
        this.rightFileAttach = rightFileAttach;
    }

    public String getRightFileAttach() 
    {
        return rightFileAttach;
    }
    public void setLeftImgAttach(String leftImgAttach) 
    {
        this.leftImgAttach = leftImgAttach;
    }

    public String getLeftImgAttach() 
    {
        return leftImgAttach;
    }
    public void setLeftFileAttach(String leftFileAttach) 
    {
        this.leftFileAttach = leftFileAttach;
    }

    public String getLeftFileAttach() 
    {
        return leftFileAttach;
    }
    public void setPath(String path) 
    {
        this.path = path;
    }

    public String getPath() 
    {
        return path;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("name", getName())
            .append("datasetId", getDatasetId())
            .append("describe", getDescribe())
            .append("objectId", getObjectId())
            .append("type", getType())
            .append("rightImgAttach", getRightImgAttach())
            .append("rightFileAttach", getRightFileAttach())
            .append("leftImgAttach", getLeftImgAttach())
            .append("leftFileAttach", getLeftFileAttach())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("path", getPath())
            .append("id", getId())
            .toString();
    }
}
