package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 站点信息表对象 sy_station
 * 
 * @author Sy
 * @date 2023-09-13
 */
public class SyStation extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 站点名称 */
    @Excel(name = "站点名称")
    private String name;

    /** 站点代码 */
    @Excel(name = "站点代码")
    private String code;

    /** 站点描述 */
    @Excel(name = "站点描述")
    private String describe;

    /** 站点状态  1-有效 2-已关停  3-已作废 */
    @Excel(name = "站点状态  1-有效 2-已关停  3-已作废")
    private Integer status;

    /** 启用日期起 */
    @Excel(name = "启用日期起")
    private String startTime;

    /** 关停日期 */
    @Excel(name = "关停日期")
    private String endTime;

    /** 站点责任人 */
    @Excel(name = "站点责任人")
    private Long chargerId;

    /** 站点责任部门 */
    @Excel(name = "站点责任部门")
    private Long deptId;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactPhone;

    private String address;

    /** 相关附件 */
    @Excel(name = "相关附件")
    private String imgAttach;

    private String fileAttach;

    /** 数据权限 */
    @Excel(name = "数据权限")
    private String roleIds;

    private String chargerName;

    private String deptName;

    private String creator;

    private List<SyFileInfo> attachList;

    private List<SyDatasetInfo> children;

    private Integer level;

    public List<SyDatasetInfo> getChildren() {
        return children;
    }

    public void setChildren(List<SyDatasetInfo> children) {
        this.children = children;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public List<SyFileInfo> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<SyFileInfo> attachList) {
        this.attachList = attachList;
    }

    public String getChargerName() {
        return chargerName;
    }

    public void setChargerName(String chargerName) {
        this.chargerName = chargerName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(String endTime) 
    {
        this.endTime = endTime;
    }

    public String getEndTime() 
    {
        return endTime;
    }
    public void setChargerId(Long chargerId) 
    {
        this.chargerId = chargerId;
    }

    public Long getChargerId() 
    {
        return chargerId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setContactPerson(String contactPerson) 
    {
        this.contactPerson = contactPerson;
    }

    public String getContactPerson() 
    {
        return contactPerson;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImgAttach() {
        return imgAttach;
    }

    public void setImgAttach(String imgAttach) {
        this.imgAttach = imgAttach;
    }

    public String getFileAttach() {
        return fileAttach;
    }

    public void setFileAttach(String fileAttach) {
        this.fileAttach = fileAttach;
    }

    public void setRoleIds(String roleIds)
    {
        this.roleIds = roleIds;
    }

    public String getRoleIds() 
    {
        return roleIds;
    }

    private Integer hasRole;
    private String ids;

    public Integer getHasRole() {
        return hasRole;
    }

    public void setHasRole(Integer hasRole) {
        this.hasRole = hasRole;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("name", getName())
            .append("code", getCode())
            .append("describe", getDescribe())
            .append("status", getStatus())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("chargerId", getChargerId())
            .append("deptId", getDeptId())
            .append("contactPerson", getContactPerson())
            .append("contactPhone", getContactPhone())
            .append("address", getAddress())
            .append("imgAttach", getImgAttach())
            .append("fileAttach", getFileAttach())
            .append("roleIds", getRoleIds())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("ids", getIds())
            .toString();
    }
}
