package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * 项目信息表对象 sy_project
 * 
 * @author Sy
 * @date 2023-09-13
 */
public class SyProject extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 项目编号 */
    @Excel(name = "项目编号")
    private String projectNo;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String name;

    /** 项目代码 */
    @Excel(name = "项目代码")
    private String code;

    /** 启动日期 */
    @Excel(name = "启动日期")
    private String startTime;

    /** 竣工日期 */
    @Excel(name = "竣工日期")
    private String endTime;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String describe;

    /** 项目状态 1-待启动	2-施工中 	3-已竣工	4-已作废 */
    @Excel(name = "项目状态 1-待启动	2-施工中 	3-已竣工	4-已作废")
    private Integer status;

    /** 项目责任人 */
    @Excel(name = "项目责任人")
    private Long chargerId;

    /** 项目责任部门 */
    @Excel(name = "项目责任部门")
    private Long deptId;

    private String chargerName;

    private String deptName;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactPhone;

    /** 项目金额 */
    @Excel(name = "项目金额")
    private BigDecimal totalInvest;

    /** 建设地点 */
    @Excel(name = "建设地点")
    private String constructSite;

    /** 监理单位 */
    @Excel(name = "监理单位")
    private String supervisor;

    /** 合同附件 */
    @Excel(name = "合同附件")
    private String contractAttach;

    /** 其他附件 */
    @Excel(name = "其他附件")
    private String materialAttach;

    /** 数据权限 */
    @Excel(name = "数据权限")
    private String roleIds;

    private String constructDept;

    private String creator;

    private List<SyDatasetInfo> children;

    private Integer level;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<SyDatasetInfo> getChildren() {
        return children;
    }

    public void setChildren(List<SyDatasetInfo> children) {
        this.children = children;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    private List<SyFileInfo> attachList;

    public List<SyFileInfo> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<SyFileInfo> attachList) {
        this.attachList = attachList;
    }

    public String getConstructDept() {
        return constructDept;
    }

    public void setConstructDept(String constructDept) {
        this.constructDept = constructDept;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setProjectNo(String projectNo) 
    {
        this.projectNo = projectNo;
    }

    public String getProjectNo() 
    {
        return projectNo;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(String endTime) 
    {
        this.endTime = endTime;
    }

    public String getEndTime() 
    {
        return endTime;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setChargerId(Long chargerId) 
    {
        this.chargerId = chargerId;
    }

    public Long getChargerId() 
    {
        return chargerId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setContactPerson(String contactPerson) 
    {
        this.contactPerson = contactPerson;
    }

    public String getContactPerson() 
    {
        return contactPerson;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }
    public void setTotalInvest(BigDecimal totalInvest) 
    {
        this.totalInvest = totalInvest;
    }

    public BigDecimal getTotalInvest() 
    {
        return totalInvest;
    }
    public void setConstructSite(String constructSite) 
    {
        this.constructSite = constructSite;
    }

    public String getConstructSite() 
    {
        return constructSite;
    }
    public void setSupervisor(String supervisor) 
    {
        this.supervisor = supervisor;
    }

    public String getSupervisor() 
    {
        return supervisor;
    }
    public void setContractAttach(String contractAttach) 
    {
        this.contractAttach = contractAttach;
    }

    public String getContractAttach() 
    {
        return contractAttach;
    }
    public void setMaterialAttach(String materialAttach) 
    {
        this.materialAttach = materialAttach;
    }

    public String getMaterialAttach() 
    {
        return materialAttach;
    }
    public void setRoleIds(String roleIds) 
    {
        this.roleIds = roleIds;
    }

    public String getRoleIds() 
    {
        return roleIds;
    }

    public String getChargerName() {
        return chargerName;
    }

    public void setChargerName(String chargerName) {
        this.chargerName = chargerName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    private String engineerIds;

    public String getEngineerIds() {
        return engineerIds;
    }

    public void setEngineerIds(String engineerIds) {
        this.engineerIds = engineerIds;
    }

    private Integer hasRole;
    private String ids;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public Integer getHasRole() {
        return hasRole;
    }

    public void setHasRole(Integer hasRole) {
        this.hasRole = hasRole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("projectNo", getProjectNo())
            .append("name", getName())
            .append("code", getCode())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("describe", getDescribe())
            .append("status", getStatus())
            .append("chargerId", getChargerId())
            .append("deptId", getDeptId())
            .append("contactPerson", getContactPerson())
            .append("contactPhone", getContactPhone())
            .append("totalInvest", getTotalInvest())
            .append("constructSite", getConstructSite())
            .append("supervisor", getSupervisor())
            .append("contractAttach", getContractAttach())
            .append("materialAttach", getMaterialAttach())
            .append("roleIds", getRoleIds())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("engineerIds", getEngineerIds())
            .append("ids", getIds())
            .toString();
    }
}
