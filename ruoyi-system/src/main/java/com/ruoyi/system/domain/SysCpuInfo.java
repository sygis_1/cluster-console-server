package com.ruoyi.system.domain;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * cpu信息对象 sys_cpu_info
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public class SysCpuInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** IP */
    @Excel(name = "IP")
    private String serverIp;

    /** CPU核数 */
    @Excel(name = "CPU核数")
    private String cpuCores;

    /** 系统使用率 */
    @Excel(name = "系统使用率")
    private String cpuSysUsedRate;

    /** 用户使用率 */
    @Excel(name = "用户使用率")
    private String cpuUserUsedRate;

    /** 空闲率 */
    @Excel(name = "空闲率")
    private String cpuWaitRate;

    /** CPU当前使用率 */
    @Excel(name = "CPU当前使用率")
    private String cpuSumUsedRate;

    /** 温度 */
    @Excel(name = "温度")
    private String cpuDegree;

    public void setServerIp(String serverIp) 
    {
        this.serverIp = serverIp;
    }

    public String getServerIp() 
    {
        return serverIp;
    }
    public void setCpuCores(String cpuCores) 
    {
        this.cpuCores = cpuCores;
    }

    public String getCpuCores() 
    {
        return cpuCores;
    }
    public void setCpuSysUsedRate(String cpuSysUsedRate) 
    {
        this.cpuSysUsedRate = cpuSysUsedRate;
    }

    public String getCpuSysUsedRate() 
    {
        return cpuSysUsedRate;
    }
    public void setCpuUserUsedRate(String cpuUserUsedRate) 
    {
        this.cpuUserUsedRate = cpuUserUsedRate;
    }

    public String getCpuUserUsedRate() 
    {
        return cpuUserUsedRate;
    }
    public void setCpuWaitRate(String cpuWaitRate) 
    {
        this.cpuWaitRate = cpuWaitRate;
    }

    public String getCpuWaitRate() 
    {
        return cpuWaitRate;
    }
    public void setCpuSumUsedRate(String cpuSumUsedRate) 
    {
        this.cpuSumUsedRate = cpuSumUsedRate;
    }

    public String getCpuSumUsedRate() 
    {
        return cpuSumUsedRate;
    }
    public void setCpuDegree(String cpuDegree) 
    {
        this.cpuDegree = cpuDegree;
    }

    public String getCpuDegree() 
    {
        return cpuDegree;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("serverIp", getServerIp())
            .append("cpuCores", getCpuCores())
            .append("cpuSysUsedRate", getCpuSysUsedRate())
            .append("cpuUserUsedRate", getCpuUserUsedRate())
            .append("cpuWaitRate", getCpuWaitRate())
            .append("cpuSumUsedRate", getCpuSumUsedRate())
            .append("cpuDegree", getCpuDegree())
            .toString();
    }
}
