package com.ruoyi.system.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 引擎信息表对象 sys_engine_info
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public class SysEngineInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 服务器IP */
    @Excel(name = "服务器IP")
    private String serverIp;

    /** 机器名 */
    @Excel(name = "机器名")
    private String hostName;

    /** 开机状态 */
    @Excel(name = "开机状态")
    private String onOff;

    /** 引擎状态 */
    @Excel(name = "引擎状态")
    private String engineStatus;

    /** 引擎类型 */
    @Excel(name = "引擎类型")
    private String engineType;

    /** 引擎类型 */
    @Excel(name = "引擎类型")
    private String typeName;

    /** 工作路径 */
    @Excel(name = "工作路径")
    private String jobPath;

    /** 服务器类型 */
    @Excel(name = "服务器类型")
    private Integer serverType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String clusterIp;

    /** 是否在集群内 */
    @Excel(name = "是否在集群内")
    private Integer clusterIn;

    /** 加入集群时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "加入集群时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 退出集群时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退出集群时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date quitTime;

    private String ccPath;

    public String getCcPath() {
        return ccPath;
    }

    public void setCcPath(String ccPath) {
        this.ccPath = ccPath;
    }

    public void setServerIp(String serverIp)
    {
        this.serverIp = serverIp;
    }

    public String getServerIp() 
    {
        return serverIp;
    }
    public void setHostName(String hostName) 
    {
        this.hostName = hostName;
    }

    public String getHostName() 
    {
        return hostName;
    }
    public void setOnOff(String onOff) 
    {
        this.onOff = onOff;
    }

    public String getOnOff() 
    {
        return onOff;
    }
    public void setEngineStatus(String engineStatus) 
    {
        this.engineStatus = engineStatus;
    }

    public String getEngineStatus() 
    {
        return engineStatus;
    }
    public void setEngineType(String engineType) 
    {
        this.engineType = engineType;
    }

    public String getEngineType() 
    {
        return engineType;
    }
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    public String getTypeName() 
    {
        return typeName;
    }
    public void setJobPath(String jobPath) 
    {
        this.jobPath = jobPath;
    }

    public String getJobPath() 
    {
        return jobPath;
    }
    public void setServerType(Integer serverType) 
    {
        this.serverType = serverType;
    }

    public Integer getServerType() 
    {
        return serverType;
    }
    public void setClusterIp(String clusterIp) 
    {
        this.clusterIp = clusterIp;
    }

    public String getClusterIp() 
    {
        return clusterIp;
    }
    public void setClusterIn(Integer clusterIn) 
    {
        this.clusterIn = clusterIn;
    }

    public Integer getClusterIn() 
    {
        return clusterIn;
    }
    public void setInTime(Date inTime) 
    {
        this.inTime = inTime;
    }

    public Date getInTime() 
    {
        return inTime;
    }
    public void setQuitTime(Date quitTime) 
    {
        this.quitTime = quitTime;
    }

    public Date getQuitTime() 
    {
        return quitTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("serverIp", getServerIp())
            .append("hostName", getHostName())
            .append("onOff", getOnOff())
            .append("engineStatus", getEngineStatus())
            .append("engineType", getEngineType())
            .append("typeName", getTypeName())
            .append("jobPath", getJobPath())
            .append("serverType", getServerType())
            .append("clusterIp", getClusterIp())
            .append("clusterIn", getClusterIn())
            .append("inTime", getInTime())
            .append("quitTime", getQuitTime())
            .toString();
    }
}
