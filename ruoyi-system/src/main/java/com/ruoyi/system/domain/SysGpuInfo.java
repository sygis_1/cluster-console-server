package com.ruoyi.system.domain;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * GPU信息表对象 sys_gpu_info
 * 
 * @author SYGIS
 * @date 2023-12-19
 */
public class SysGpuInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** IP */
    @Excel(name = "IP")
    private String serverIp;

    /** 序号 */
    @Excel(name = "序号")
    private String gpuNo;

    /** 名称 */
    @Excel(name = "名称")
    private String gpuName;

    /** 总显存 */
    @Excel(name = "总显存")
    private String gpuSumMem;

    /** 总显存 */
    @Excel(name = "总显存")
    private Long gpuTotalMen;

    /** 已使用显存 */
    @Excel(name = "已使用显存")
    private String gpuUsedMem;

    /** 使用率 */
    @Excel(name = "使用率")
    private String gpuUsedRate;

    /** 空闲显存 */
    @Excel(name = "空闲显存")
    private String gpuFreeMem;

    /** 温度 */
    @Excel(name = "温度")
    private String gpuDegree;

    public void setServerIp(String serverIp) 
    {
        this.serverIp = serverIp;
    }

    public String getServerIp() 
    {
        return serverIp;
    }
    public void setGpuNo(String gpuNo) 
    {
        this.gpuNo = gpuNo;
    }

    public String getGpuNo() 
    {
        return gpuNo;
    }
    public void setGpuName(String gpuName) 
    {
        this.gpuName = gpuName;
    }

    public String getGpuName() 
    {
        return gpuName;
    }
    public void setGpuSumMem(String gpuSumMem) 
    {
        this.gpuSumMem = gpuSumMem;
    }

    public String getGpuSumMem() 
    {
        return gpuSumMem;
    }
    public void setGpuTotalMen(Long gpuTotalMen) 
    {
        this.gpuTotalMen = gpuTotalMen;
    }

    public Long getGpuTotalMen() 
    {
        return gpuTotalMen;
    }
    public void setGpuUsedMem(String gpuUsedMem) 
    {
        this.gpuUsedMem = gpuUsedMem;
    }

    public String getGpuUsedMem() 
    {
        return gpuUsedMem;
    }
    public void setGpuUsedRate(String gpuUsedRate) 
    {
        this.gpuUsedRate = gpuUsedRate;
    }

    public String getGpuUsedRate() 
    {
        return gpuUsedRate;
    }
    public void setGpuFreeMem(String gpuFreeMem) 
    {
        this.gpuFreeMem = gpuFreeMem;
    }

    public String getGpuFreeMem() 
    {
        return gpuFreeMem;
    }
    public void setGpuDegree(String gpuDegree) 
    {
        this.gpuDegree = gpuDegree;
    }

    public String getGpuDegree() 
    {
        return gpuDegree;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("serverIp", getServerIp())
            .append("gpuNo", getGpuNo())
            .append("gpuName", getGpuName())
            .append("gpuSumMem", getGpuSumMem())
            .append("gpuTotalMen", getGpuTotalMen())
            .append("gpuUsedMem", getGpuUsedMem())
            .append("gpuUsedRate", getGpuUsedRate())
            .append("gpuFreeMem", getGpuFreeMem())
            .append("gpuDegree", getGpuDegree())
            .toString();
    }
}
