package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * 工程信息表对象 sy_engineer
 * 
 * @author Sy
 * @date 2023-09-13
 */
public class SyEngineer extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 工程编号 */
    @Excel(name = "工程编号")
    private String engineerNo;

    /** 工程名称 */
    @Excel(name = "工程名称")
    private String name;

    /** 项目标识 */
    @Excel(name = "项目标识")
    private String projectId;

    private String projectName;

    /** 工程代码 */
    @Excel(name = "工程代码")
    private String code;

    /** 启动日期 */
    @Excel(name = "启动日期")
    private String startTime;

    /** 竣工日期 */
    @Excel(name = "竣工日期")
    private String endTime;

    /** 工程描述 */
    @Excel(name = "工程描述")
    private String describe;

    /** 工程状态 1-待启动	2-施工中 	3-已竣工 4-已作废 */
    @Excel(name = "工程状态 1-待启动	2-施工中 	3-已竣工 4-已作废")
    private Integer status;

    /** 工程责任人 */
    @Excel(name = "工程责任人")
    private Long chargerId;

    /** 工程责任部门 */
    @Excel(name = "工程责任部门")
    private Long deptId;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactPhone;

    /** 工程总造价 */
    @Excel(name = "工程总造价")
    private BigDecimal costConstruction;

    /** 施工单位 */
    @Excel(name = "施工单位")
    private String ownerDept;

    /** 施工单位联系人 */
    @Excel(name = "施工单位联系人")
    private String ownerContact;

    /** 监理单位 */
    @Excel(name = "监理单位")
    private String supervisor;

    /** 合同附件 */
    @Excel(name = "合同附件")
    private String contractAttach;

    /** 其他附件 */
    @Excel(name = "其他附件")
    private String materialAttach;

    /** 数据权限 */
    @Excel(name = "数据权限")
    private String roleIds;

    private String chargerName;

    private String deptName;

    private String creator;

    private List<SyFileInfo> attachList;

    private String constructSite;

    private Integer hasProject;

    private String subEngineerIds;

    private List<SyDatasetInfo> children;

    private Integer level;

    public List<SyDatasetInfo> getChildren() {
        return children;
    }

    public void setChildren(List<SyDatasetInfo> children) {
        this.children = children;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getSubEngineerIds() {
        return subEngineerIds;
    }

    public void setSubEngineerIds(String subEngineerIds) {
        this.subEngineerIds = subEngineerIds;
    }

    public Integer getHasProject() {
        return hasProject;
    }

    public void setHasProject(Integer hasProject) {
        this.hasProject = hasProject;
    }

    public String getConstructSite() {
        return constructSite;
    }

    public void setConstructSite(String constructSite) {
        this.constructSite = constructSite;
    }

    public List<SyFileInfo> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<SyFileInfo> attachList) {
        this.attachList = attachList;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getChargerName() {
        return chargerName;
    }

    public void setChargerName(String chargerName) {
        this.chargerName = chargerName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setEngineerNo(String engineerNo) 
    {
        this.engineerNo = engineerNo;
    }

    public String getEngineerNo() 
    {
        return engineerNo;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setProjectId(String projectId) 
    {
        this.projectId = projectId;
    }

    public String getProjectId() 
    {
        return projectId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(String endTime) 
    {
        this.endTime = endTime;
    }

    public String getEndTime() 
    {
        return endTime;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setChargerId(Long chargerId) 
    {
        this.chargerId = chargerId;
    }

    public Long getChargerId() 
    {
        return chargerId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setContactPerson(String contactPerson) 
    {
        this.contactPerson = contactPerson;
    }

    public String getContactPerson() 
    {
        return contactPerson;
    }
    public void setContactPhone(String contactPhone) 
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() 
    {
        return contactPhone;
    }
    public void setCostConstruction(BigDecimal costConstruction) 
    {
        this.costConstruction = costConstruction;
    }

    public BigDecimal getCostConstruction() 
    {
        return costConstruction;
    }
    public void setOwnerDept(String ownerDept) 
    {
        this.ownerDept = ownerDept;
    }

    public String getOwnerDept() 
    {
        return ownerDept;
    }
    public void setOwnerContact(String ownerContact) 
    {
        this.ownerContact = ownerContact;
    }

    public String getOwnerContact() 
    {
        return ownerContact;
    }
    public void setSupervisor(String supervisor) 
    {
        this.supervisor = supervisor;
    }

    public String getSupervisor() 
    {
        return supervisor;
    }
    public void setContractAttach(String contractAttach) 
    {
        this.contractAttach = contractAttach;
    }

    public String getContractAttach() 
    {
        return contractAttach;
    }
    public void setMaterialAttach(String materialAttach) 
    {
        this.materialAttach = materialAttach;
    }

    public String getMaterialAttach() 
    {
        return materialAttach;
    }
    public void setRoleIds(String roleIds) 
    {
        this.roleIds = roleIds;
    }

    public String getRoleIds() 
    {
        return roleIds;
    }

    private Integer hasRole;
    private String ids;

    public Integer getHasRole() {
        return hasRole;
    }

    public void setHasRole(Integer hasRole) {
        this.hasRole = hasRole;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("engineerNo", getEngineerNo())
            .append("name", getName())
            .append("projectId", getProjectId())
            .append("code", getCode())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("describe", getDescribe())
            .append("status", getStatus())
            .append("chargerId", getChargerId())
            .append("deptId", getDeptId())
            .append("contactPerson", getContactPerson())
            .append("contactPhone", getContactPhone())
            .append("costConstruction", getCostConstruction())
            .append("ownerDept", getOwnerDept())
            .append("ownerContact", getOwnerContact())
            .append("supervisor", getSupervisor())
            .append("contractAttach", getContractAttach())
            .append("materialAttach", getMaterialAttach())
            .append("roleIds", getRoleIds())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("ids", getIds())
            .toString();
    }
}
