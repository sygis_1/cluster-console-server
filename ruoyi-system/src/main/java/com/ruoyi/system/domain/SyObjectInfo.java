package com.ruoyi.system.domain;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 数据对象对象 sy_object_info
 * 
 * @author Sy
 * @date 2023-09-26
 */
public class SyObjectInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 对象名称 */
    @Excel(name = "对象名称")
    private String name;

    /** 数据集标识 */
    @Excel(name = "数据集标识")
    private String datasetId;

    /** 对象描述 */
    @Excel(name = "对象描述")
    private String describe;

    /** 中心点坐标X */
    @Excel(name = "中心点坐标X")
    private String centerX;

    /** 中心点坐标Y */
    @Excel(name = "中心点坐标Y")
    private String centerY;

    /** 浏览高度 */
    @Excel(name = "浏览高度")
    private BigDecimal centerH;

    /** 角度 */
    @Excel(name = "角度")
    private Integer angle;

    /** 部件归类 */
    @Excel(name = "部件归类")
    private Integer type;

    /** 右幅图片附件 */
    @Excel(name = "右幅图片附件")
    private String rightImgAttach;

    /** 右幅其他附件 */
    @Excel(name = "右幅其他附件")
    private String rightFileAttach;

    /** 左幅图片附件 */
    @Excel(name = "左幅图片附件")
    private String leftImgAttach;

    /** 左幅其他附件 */
    @Excel(name = "左幅其他附件")
    private String leftFileAttach;

    /** 访问路径 */
    @Excel(name = "访问路径")
    private String path;

    private String creator;

    private String caption;

    private Integer level;

    private String datasetName;

    private String datasetCode;

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public String getDatasetCode() {
        return datasetCode;
    }

    public void setDatasetCode(String datasetCode) {
        this.datasetCode = datasetCode;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDatasetId(String datasetId) 
    {
        this.datasetId = datasetId;
    }

    public String getDatasetId() 
    {
        return datasetId;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setCenterX(String centerX) 
    {
        this.centerX = centerX;
    }

    public String getCenterX() 
    {
        return centerX;
    }
    public void setCenterY(String centerY) 
    {
        this.centerY = centerY;
    }

    public String getCenterY() 
    {
        return centerY;
    }
    public void setCenterH(BigDecimal centerH) 
    {
        this.centerH = centerH;
    }

    public BigDecimal getCenterH() 
    {
        return centerH;
    }
    public void setAngle(Integer angle) 
    {
        this.angle = angle;
    }

    public Integer getAngle() 
    {
        return angle;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setRightImgAttach(String rightImgAttach) 
    {
        this.rightImgAttach = rightImgAttach;
    }

    public String getRightImgAttach() 
    {
        return rightImgAttach;
    }
    public void setRightFileAttach(String rightFileAttach) 
    {
        this.rightFileAttach = rightFileAttach;
    }

    public String getRightFileAttach() 
    {
        return rightFileAttach;
    }
    public void setLeftImgAttach(String leftImgAttach) 
    {
        this.leftImgAttach = leftImgAttach;
    }

    public String getLeftImgAttach() 
    {
        return leftImgAttach;
    }
    public void setLeftFileAttach(String leftFileAttach) 
    {
        this.leftFileAttach = leftFileAttach;
    }

    public String getLeftFileAttach() 
    {
        return leftFileAttach;
    }
    public void setPath(String path) 
    {
        this.path = path;
    }

    public String getPath() 
    {
        return path;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("name", getName())
            .append("datasetId", getDatasetId())
            .append("describe", getDescribe())
            .append("centerX", getCenterX())
            .append("centerY", getCenterY())
            .append("centerH", getCenterH())
            .append("angle", getAngle())
            .append("type", getType())
            .append("rightImgAttach", getRightImgAttach())
            .append("rightFileAttach", getRightFileAttach())
            .append("leftImgAttach", getLeftImgAttach())
            .append("leftFileAttach", getLeftFileAttach())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("path", getPath())
            .append("caption", getCaption())
            .toString();
    }
}
