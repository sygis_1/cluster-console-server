package com.ruoyi.system.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 数据集对象 sy_dataset_info
 * 
 * @author Sy
 * @date 2023-09-26
 */
public class SyDatasetInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /** 主键标识 */
    private String uuid;

    /** 数据集名称 */
    @Excel(name = "数据集名称")
    private String name;

    /** 数据集编码 */
    @Excel(name = "数据集编码")
    private String code;

    /** 数据集类型 1-三维数据 2-BIM数据 3-矢量数据 4-栅格数据 5-其他非结构化数据 */
    @Excel(name = "数据集类型 1-三维数据 2-BIM数据 3-矢量数据 4-栅格数据 5-其他非结构化数据")
    private Integer type;

    /** 数据集描述 */
    @Excel(name = "数据集描述")
    private String describe;

    /** 数据集发布状态 0-待发布 1-已发布 2-已下架 */
    @Excel(name = "数据集发布状态 0-待发布 1-已发布 2-已下架")
    private Integer status;

    /** 访问路径 */
    @Excel(name = "访问路径")
    private String path;

    /** 项目标识 */
    @Excel(name = "项目标识")
    private String projectIds;

    /** 工程标识 */
    @Excel(name = "工程标识")
    private String engineerIds;

    /** 子工程标识 */
    @Excel(name = "子工程标识")
    private String subEngineerIds;

    /** 站点标识 */
    @Excel(name = "站点标识")
    private String stationIds;

    /** 角色标识 */
    @Excel(name = "角色标识")
    private String roleIds;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishTime;

    /** 发布人员 */
    @Excel(name = "发布人员")
    private Long publishId;

    private List<SyObjectInfo> children;

    private Integer level;

    public List<SyObjectInfo> getChildren() {
        return children;
    }

    public void setChildren(List<SyObjectInfo> children) {
        this.children = children;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setPath(String path) 
    {
        this.path = path;
    }

    public String getPath() 
    {
        return path;
    }
    public void setProjectIds(String projectIds) 
    {
        this.projectIds = projectIds;
    }

    public String getProjectIds() 
    {
        return projectIds;
    }
    public void setEngineerIds(String engineerIds) 
    {
        this.engineerIds = engineerIds;
    }

    public String getEngineerIds() 
    {
        return engineerIds;
    }
    public void setSubEngineerIds(String subEngineerIds) 
    {
        this.subEngineerIds = subEngineerIds;
    }

    public String getSubEngineerIds() 
    {
        return subEngineerIds;
    }
    public void setStationIds(String stationIds) 
    {
        this.stationIds = stationIds;
    }

    public String getStationIds() 
    {
        return stationIds;
    }
    public void setRoleIds(String roleIds) 
    {
        this.roleIds = roleIds;
    }

    public String getRoleIds() 
    {
        return roleIds;
    }
    public void setPublishTime(Date publishTime) 
    {
        this.publishTime = publishTime;
    }

    public Date getPublishTime() 
    {
        return publishTime;
    }
    public void setPublishId(Long publishId) 
    {
        this.publishId = publishId;
    }

    public Long getPublishId() 
    {
        return publishId;
    }

    private String publisher;
    private String creator;
    private String updator;
    private Integer hasProject;
    private String ids;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public Integer getHasProject() {
        return hasProject;
    }

    public void setHasProject(Integer hasProject) {
        this.hasProject = hasProject;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("name", getName())
            .append("code", getCode())
            .append("type", getType())
            .append("describe", getDescribe())
            .append("status", getStatus())
            .append("path", getPath())
            .append("projectIds", getProjectIds())
            .append("engineerIds", getEngineerIds())
            .append("subEngineerIds", getSubEngineerIds())
            .append("stationIds", getStationIds())
            .append("roleIds", getRoleIds())
            .append("publishTime", getPublishTime())
            .append("publishId", getPublishId())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("ids", getIds())
            .toString();
    }
}
